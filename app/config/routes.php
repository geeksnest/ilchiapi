<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.

  $routes[] = [
  'method' => 'post',
  'route' => '/api/update',
  'handler' => 'myFunction'
  ];

 */


$routes[] = [
    'method' => 'get',
    'route' => '/site/maintenace',
    'handler' => ['Controllers\SettingsController', 'siteMaintenaceAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/site/logo',
    'handler' => ['Controllers\SettingsController', 'siteLogoAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'post',
    'route' => '/members/registration',
    'handler' => ['Controllers\MembersController', 'registrationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/memberslist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\MembersController', 'memberslistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/checkuser',
    'handler' => ['Controllers\MembersController', 'checkuserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/updatedonations',
    'handler' => ['Controllers\MembersController', 'donationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/userdonation',
    'handler' => ['Controllers\MembersController', 'userdonationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/sendmail',
    'handler' => ['Controllers\MembersController', 'sendmailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/members/memberinfo/{memberid}',
    'handler' => ['Controllers\MembersController', 'membersinfoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberupdate/{memberid}',
    'handler' => ['Controllers\MembersController', 'memberupdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberupdatepassword/{memberid}',
    'handler' => ['Controllers\MembersController', 'memberupdatepasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/members/memberdelete/{memberid}',
    'handler' => ['Controllers\MembersController', 'memberdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/test/{id}',
    'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
    'method' => 'post',
    'route' => '/peacemap/create',
    'handler' => ['Controllers\PeacemapController', 'createAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/peacemap/update',
    'handler' => ['Controllers\PeacemapController', 'updateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/peacemap/list',
    'handler' => ['Controllers\PeacemapController', 'listAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/peacemap/getmap/{id}',
    'handler' => ['Controllers\PeacemapController', 'getmapAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/peacemap/delete/{id}',
    'handler' => ['Controllers\PeacemapController', 'deleteAction'],
    'authentication' => FALSE
];




//kyben
$routes[] = [
	'method' => 'get', 
	'route' => '/validate/album/{id}', 
	'handler' => ['Controllers\FileuploaderController', 'validateAlbumAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/sliderimage/slider', 
	'handler' => ['Controllers\SliderimageController', 'slideruploadAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/ajaxfileuploader/{filename}/{folderName}/{folderid}/{date}', 
	'handler' => ['Controllers\FileuploaderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/listsliderimages/{getid}', 
	'handler' => ['Controllers\FileuploaderController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/imginfoupdate', 
	'handler' => ['Controllers\FileuploaderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/dltphoto', 
	'handler' => ['Controllers\FileuploaderController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/generateid', 
	'handler' => ['Controllers\FileuploaderController', 'folderidAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/testimonials/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\TestimonialsController', 'testimonialAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/testimonial/{offset}', 
	'handler' => ['Controllers\TestimonialsController', 'showTestimonialAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/dlt/{id}', 
	'handler' => ['Controllers\TestimonialsController', 'dltAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/view/{id}', 
	'handler' => ['Controllers\TestimonialsController', 'infoviewAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/status/{id}/{status}', 
	'handler' => ['Controllers\TestimonialsController', 'updatestatusAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/updatetestimony/{id}/{message}', 
	'handler' => ['Controllers\TestimonialsController', 'updateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/submit', 
	'handler' => ['Controllers\TestimonialsController', 'submitAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/utility/getpage', 
	'handler' => ['Controllers\MenuCreatorController', 'getPageAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/savemenu', 
	'handler' => ['Controllers\MenuCreatorController', 'saveAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/listmenu', 
	'handler' => ['Controllers\MenuCreatorController', 'listmenuAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/submenu', 
	'handler' => ['Controllers\MenuCreatorController', 'submenuAction'],
	'authentication' => FALSE
];


////ILCHI KYBEN
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/videouploader/{filename}', 
	'handler' => ['Controllers\VideoController', 'videouploaderAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post', 
	'route' => '/utility/tempvid', 
	'handler' => ['Controllers\VideoController', 'tempvidAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post', 
	'route' => '/utility/savevid', 
	'handler' => ['Controllers\VideoController', 'savevidAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/videolist/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\VideoController', 'videolistdAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'get', 
	'route' => '/video/setasfeat/{id}', 
	'handler' => ['Controllers\VideoController', 'setasfeatAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/video/removefeat/{id}', 
	'handler' => ['Controllers\VideoController', 'removefeatAction'],
	'authentication' => FALSE
];



$routes[] = [
	'method' => 'get', 
	'route' => '/utility/videodisp/{offset}', 
	'handler' => ['Controllers\VideoController', 'videoindexAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/dltvideo/{id}', 
	'handler' => ['Controllers\VideoController', 'dltvideoAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post', 
	'route' => '/utility/updatevid', 
	'handler' => ['Controllers\VideoController', 'updatevidAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/getdisp/{id}', 
	'handler' => ['Controllers\VideoController', 'getdispAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/videoshow/{id}', 
	'handler' => ['Controllers\VideoController', 'videoshowAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/showmore/{album}', 
	'handler' => ['Controllers\VideoController', 'showmoreAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/slider/{filename}/{folderName}/{folderid}', 
	'handler' => ['Controllers\SliderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/sliderlist/{getid}', 
	'handler' => ['Controllers\SliderController', 'imagelistAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/slideralbum/{albumname}/{albumid}', 
	'handler' => ['Controllers\SliderController', 'albumCreatection'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/sliderlistview/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\SliderViewController', 'managealbumAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/deletealbum/{albumname}', 
	'handler' => ['Controllers\SliderViewController', 'deletealbumAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/slideralbumdelete/{albumname}', 
	'handler' => ['Controllers\SliderViewController', 'deletealbumAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/editslider/{albumid}', 
	'handler' => ['Controllers\SliderViewController', 'albuminfoAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/editlabumname/{id}/{name}', 
	'handler' => ['Controllers\SliderViewController', 'editlabumnameAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/sliderimagelist/{getid}', 
	'handler' => ['Controllers\SliderViewController', 'imagelistAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/sliderbanner', 
	'handler' => ['Controllers\SliderViewController', 'bannertAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/utility/sliderphotoupdate', 
	'handler' => ['Controllers\SliderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post', 
	'route' => '/utility/dltsliderphoto', 
	'handler' => ['Controllers\SliderController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/latestvid', 
	'handler' => ['Controllers\VideoController', 'latestvidAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/setslider/{id}', 
	'handler' => ['Controllers\SliderController', 'setsliderAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/featuredvid', 
	'handler' => ['Controllers\VideoController', 'featuredvidAction'],
	'authentication' => FALSE
];
// visit
$routes[] = [
	'method' => 'get', 
	'route' => '/getip/visit/{ip}', 
	'handler' => ['Controllers\VisitController', 'visitAction'],
	'authentication' => FALSE
];
//Total Visits
$routes[] = [
	'method' => 'get', 
	'route' => '/visit/count', 
	'handler' => ['Controllers\VisitController', 'countAction'],
	'authentication' => FALSE
];
//Total Visits
$routes[] = [
	'method' => 'get', 
	'route' => '/visit/diagram', 
	'handler' => ['Controllers\VisitController', 'diagramAction'],
	'authentication' => FALSE
];
//Total News
$routes[] = [
	'method' => 'get', 
	'route' => '/news/count', 
	'handler' => ['Controllers\VisitController', 'newsAction'],
	'authentication' => FALSE
];
//Total Video
$routes[] = [
	'method' => 'get', 
	'route' => '/video/count', 
	'handler' => ['Controllers\VisitController', 'VideoAction'],
	'authentication' => FALSE
];
//Total Video
$routes[] = [
	'method' => 'get', 
	'route' => '/image/count', 
	'handler' => ['Controllers\VisitController', 'imageAction'],
	'authentication' => FALSE
];
//Total Video
$routes[] = [
	'method' => 'get', 
	'route' => '/imagesize/count', 
	'handler' => ['Controllers\VisitController', 'imagesizeAction'],
	'authentication' => FALSE
];

//Total Video
$routes[] = [
	'method' => 'get', 
	'route' => '/system/performance', 
	'handler' => ['Controllers\VisitController', 'sysperAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'get', 
	'route' => '/proj/featproject', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featprojAction'],
	'authentication' => FALSE
];












$routes[] = [
	'method' => 'get', 
	'route' => '/utility/publicationUploadImage/{filename}/{type}', 
	'handler' => ['Controllers\PublicationController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/bannerlist', 
	'handler' => ['Controllers\PublicationController', 'imagelistAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/bannerdelete/{id}', 
	'handler' => ['Controllers\PublicationController', 'dltphotoAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post', 
	'route' => '/utility/create', 
	'handler' => ['Controllers\PublicationController', 'createPageAction'],
	'authentication' => FALSE
];

// $routes[] = [
// 	'method' => 'post', 
// 	'route' => '/utility/create', 
// 	'handler' => ['Controllers\PublicationController', 'createPageAction'],
// 	'authentication' => FALSE
// ];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/publication', 
	'handler' => ['Controllers\PublicationController', 'publistAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/managepub/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\PublicationController', 'managepubAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/deletepub/{id}', 
	'handler' => ['Controllers\PublicationController', 'deletepubAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/listeditpub/{id}', 
	'handler' => ['Controllers\PublicationController', 'dispeditAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/utility/updatepub', 
	'handler' => ['Controllers\PublicationController', 'updatepubAction'],
	'authentication' => FALSE
];



//MAINTENANCE
$routes[] = [
    'method' => 'post',
    'route' => '/settings/managesettings',
    'handler' => ['Controllers\SettingsController', 'managesettingsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/savelogo/{path}',
    'handler' => ['Controllers\SettingsController', 'savelogoAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/on/{id}', 
	'handler' => ['Controllers\SettingsController', 'maintenanceAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/settings/maintenanceon',  
	'handler' => ['Controllers\SettingsController', 'maintenanceonAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/off/{id}', 
	'handler' => ['Controllers\SettingsController', 'maintenanceAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/settings/maintenanceoff',  
	'handler' => ['Controllers\SettingsController', 'maintenanceoffAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'post', 
	'route' => '/settings/googleanalytics',  
	'handler' => ['Controllers\SettingsController', 'scriptAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/loadscript',  
	'handler' => ['Controllers\SettingsController', 'loadscriptAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/settings/script',  
	'handler' => ['Controllers\SettingsController', 'displaytAction'],
	'authentication' => FALSE
];
// END MAINTENANCE




//rainier pages


$routes[] = [
	'method' => 'get', 
	'route' => '/pages/ajaxfileuploader/{filename}/{type}', 
	'handler' => ['Controllers\ImageuploaderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/listpageimages', 
	'handler' => ['Controllers\ImageuploaderController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pages/imginfoupdate', 
	'handler' => ['Controllers\ImageuploaderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pages/dltpagephoto', 
	'handler' => ['Controllers\ImageuploaderController', 'dltphotoAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/pages/create', 
	'handler' => ['Controllers\PagesController', 'createPageAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\PagesController', 'managepagesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/editpage/{pageid}', 
	'handler' => ['Controllers\PagesController', 'pageinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/pages/updatepage', 
	'handler' => ['Controllers\PagesController', 'pageUpdateAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/page/pagedelete/{pageid}', 
	'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/getpage/{pageid}',
	'handler' => ['Controllers\PagesController', 'getPageAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/deletealbum/{albumname}', 
	'handler' => ['Controllers\viewalbumController', 'deletealbumAction'],
	'authentication' => FALSE
];



$routes[] = [
	'method' => 'get', 
	'route' => '/pages/page404/{slug}',
	'handler' => ['Controllers\PagesController', 'page404Action'],
	'authentication' => FALSE
];





//rainier news



$routes[] = [
	'method' => 'get', 
	'route' => '/news/ajaxfileuploader/{filename}/{type}', 
	'handler' => ['Controllers\NewsImageuploaderController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/listnewsimages', 
	'handler' => ['Controllers\NewsImageuploaderController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/imginfoupdate', 
	'handler' => ['Controllers\NewsImageuploaderController', 'updateinfoimgAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/dltpagephoto', 
	'handler' => ['Controllers\NewsImageuploaderController', 'dltphotoAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/news/loadcategory', 
	'handler' => ['Controllers\NewsController', 'loadcategoryAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/createalbum/{albumname}/{albumid}/{description}/{date}', 
	'handler' => ['Controllers\FileuploaderController', 'createalbumAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/utility/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\viewalbumController', 'managealbumAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/editalbum/{albumid}', 
	'handler' => ['Controllers\viewalbumController', 'albuminfoAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/updatealbum/{filename}', 
	'handler' => ['Controllers\viewalbumController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/utility/deletealbum/{albumname}', 
	'handler' => ['Controllers\viewalbumController', 'deletealbumAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/news/create', 
	'handler' => ['Controllers\NewsController', 'createNewsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\NewsController', 'managenewsAction'],
	'authentication' => FALSE
];



$routes[] = [
	'method' => 'get', 
	'route' => '/news/listcategory', 
	'handler' => ['Controllers\NewsController', 'listcategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/newsdelete/{newsid}', 
	'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/editnews/{newsid}', 
	'handler' => ['Controllers\NewsController', 'newsinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/updatenews', 
	'handler' => ['Controllers\NewsController', 'newsUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/savetags', 
	'handler' => ['Controllers\NewsController', 'createtagsAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/news/news/{offset}', 
 'handler' => ['Controllers\NewsController', 'showNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/fullnews/{newsslugs}', 
	'handler' => ['Controllers\NewsController', 'fullnewsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/managetags/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\NewsController', 'managetagsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/tagsdelete/{id}', 
	'handler' => ['Controllers\NewsController', 'tagsdeleteAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/news/updatetagnames/{catname}/{id}',
	'handler' => ['Controllers\NewsController', 'tagsUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/create',
	'handler' => ['Controllers\NewsLetterController', 'createNewsLetterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\NewsLetterController', 'managenewsletterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/newsletterdelete/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'newsletterdeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/editnewsletter/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'newsletterinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/updatenewsletter', 
	'handler' => ['Controllers\NewsLetterController', 'newsletterUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/subscriberslist/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'subscriberslistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/sendnewsletter/{NSemail}', 
	'handler' => ['Controllers\NewsLetterController', 'sendnewsletterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/memberlist/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'memberlistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/ajaxfileuploader/{filename}', 
	'handler' => ['Controllers\NewsLetterController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/lstphoto', 
	'handler' => ['Controllers\NewsLetterController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/newsletter/dltphoto', 
	'handler' => ['Controllers\NewsLetterController', 'dltphotoAction'],
	'authentication' => FALSE
];


$routes[] = [
 'method' => 'get', 
 'route' => '/album/album/{offset}', 
 'handler' => ['Controllers\AlbumController', 'showAlbumAction'],
 'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/gallery/folder/{id}', 
	'handler' => ['Controllers\AlbumController', 'folderAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/post/{offset}', 
 'handler' => ['Controllers\NewsController', 'showPostAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/newsrss/{offset}', 
 'handler' => ['Controllers\NewsController', 'newrssAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/category/{offset}/{category}', 
 'handler' => ['Controllers\NewsController', 'showNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/categoryrelated/{offset}/{category}/{newsid}', 
 'handler' => ['Controllers\NewsController', 'showNewsrelatedAction'],
 'authentication' => FALSE
];


$routes[] = [
	'method' => 'get', 
	'route' => '/news/managecategory/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\NewsController', 'managecategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/categorydelete/{id}', 
	'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/updatecategorynames/{catname}/{id}',
	'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/news/categorylist', 
	'handler' => ['Controllers\NewsController', 'categorylistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/savecategory', 
	'handler' => ['Controllers\NewsController', 'createcategoryAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/news/savetag', 
	'handler' => ['Controllers\NewsController', 'createtagsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsslug/check/{slug}', 
	'handler' => ['Controllers\NewsController', 'newsSlugAction'],
	'authentication' => FALSE
];


$routes[] = [
 'method' => 'get', 
 'route' => '/project/project/{offset}', 
 'handler' => ['Controllers\FeaturedprojectsController', 'showProjectAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/project/fullproject/{offset}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'fullprojectsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/project/featuredproject', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featuredprojectsAction'],
	'authentication' => FALSE
];
$routes[] = [
 'method' => 'get', 
 'route' => '/post/latestpost/{offset}', 
 'handler' => ['Controllers\NewsController', 'showLatestPostAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/indexmanagefeature/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'indexmanagefeaturedprojectAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/contacts/send', 
	'handler' => ['Controllers\ContactsController', 'sendMessageAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/journal/{offset}', 
 'handler' => ['Controllers\NewsController', 'showJournalPostAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/showtags', 
 'handler' => ['Controllers\NewsController', 'showTagsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/relatedtags/{categoryid}', 
 'handler' => ['Controllers\NewsController', 'relatedTagsAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/contacts/listreplies/{id}', 
	'handler' => ['Controllers\ProposalsController', 'listreplyAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/newsletter/newsletter/{offset}', 
 'handler' => ['Controllers\NewsLetterController', 'shownewsletterAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/newsletter/fullletter/{newsletterid}', 
	'handler' => ['Controllers\NewsLetterController', 'fullnewsletterAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/post/updateviews/{newsslugs}', 
	'handler' => ['Controllers\NewsController', 'countUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/post/popularpost/{offset}', 
 'handler' => ['Controllers\NewsController', 'popularpostAction'],
 'authentication' => FALSE
];


$routes[] = [
	'method' => 'get', 
	'route' => '/user/login/{username}/{password}', 
	'handler' => ['Controllers\UserController', 'adminLoginAction'],
	'authentication' => FALSE
];




$routes[] = [
	'method' => 'post', 
	'route' => '/forgotpassword/send/{email}', 
	'handler' => ['Controllers\UserController', 'forgotpasswordAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/checktoken/check/{email}', 
	'handler' => ['Controllers\UserController', 'checktokenAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/updatepassword/token', 
	'handler' => ['Controllers\UserController', 'updatepasswordtokenAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/user/formrole', 
	'handler' => ['Controllers\UserController', 'formRoleAction'],
	'authentication' => FALSE
];


$routes[] = [
 'method' => 'get', 
 'route' => '/search/search/{keyword}/{offset}', 
 'handler' => ['Controllers\SearchController', 'searchAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/page/page/{keyword}/{offset}', 
 'handler' => ['Controllers\SearchController', 'pageAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get', 
 'route' => '/searchauto/searchauto', 
 'handler' => ['Controllers\SearchController', 'searchautoAction'],
 'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/album/updatealbumname/{albumname}/{albumid}',
	'handler' => ['Controllers\FileuploaderController', 'updatealbumnameAction'],
	'authentication' => FALSE
];






// uson


$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/listfiles', 
	'handler' => ['Controllers\ProposalsController', 'filelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/proposals/create', 
	'handler' => ['Controllers\ProposalsController', 'submitproposalAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/manage/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\ProposalsController', 'manageproposalsAction'],
	'authentication' => FALSE
];
$routes[] = [
	'method' => 'get', 
	'route' => '/proposals/view/{id}', 
	'handler' => ['Controllers\ProposalsController', 'viewproposalAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/proposals/reply', 
	'handler' => ['Controllers\ProposalsController', 'replyproposalAction'],
	'authentication' => FALSE
];

// unahan ng rota ni jimmy

// featured project

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/insdata',
	'handler' => ['Controllers\FeaturedprojectsController', 'featdataAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/ajaxfileuploader/{filename}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/listsimg', 
	'handler' => ['Controllers\FeaturedprojectsController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/slidelist', 
	'handler' => ['Controllers\FeaturedprojectsController', 'slidesAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/dltpagephoto', 
	'handler' => ['Controllers\FeaturedprojectsController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/dltfeatphoto', 
	'handler' => ['Controllers\FeaturedprojectsController', 'dltfeatphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/managefeature/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'managefeaturedprojectAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/editfeature/{pageid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featureinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/updatefeature', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featureUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/featuredelete/{pageid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featuredeleteAction'],
	'authentication' => FALSE
];

// front end routes

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/viewfeature', 
	'handler' => ['Controllers\FeaturedprojectsController', 'featureviewAction'],
	'authentication' => FALSE
];

// offset (show more)

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/lispro/{offset}',
	'handler' => ['Controllers\FeaturedprojectsController', 'featuredsAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/featuredprojects/listact/{pageid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'listactAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/featuredprojects/projectslider/{featid}', 
	'handler' => ['Controllers\FeaturedprojectsController', 'projectsliderAction'],
	'authentication' => FALSE
];

// calendar

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/insdata',
	'handler' => ['Controllers\CalendarController', 'addactAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/ajaxfileuploader/{filename}', 
	'handler' => ['Controllers\CalendarController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/listsimg', 
	'handler' => ['Controllers\CalendarController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/dltpagephoto', 
	'handler' => ['Controllers\CalendarController', 'dltphotoAction'],
	'authentication' => FALSE
];


$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/dltfeatphoto', 
	'handler' => ['Controllers\CalendarController', 'dltactphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/manageactivity', 
	'handler' => ['Controllers\CalendarController', 'viewcalendarAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/listview', 
	'handler' => ['Controllers\CalendarController', 'listviewAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/calendar/editactivity/{actid}',
	'handler' => ['Controllers\CalendarController', 'activityinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/updateactivity', 
	'handler' => ['Controllers\CalendarController', 'activityUpdateAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/calendar/activitydelete/{actid}', 
	'handler' => ['Controllers\CalendarController', 'activitydeleteAction'],
	'authentication' => FALSE
];

// subscriber+

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/addNMS', 
	'handler' => ['Controllers\SubscribersController', 'nmsaddAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/insdata', 
	'handler' => ['Controllers\SubscribersController', 'addsubscriberAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/subscribers/subscriberslist/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\SubscribersController', 'subscriberslistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/subscribers/editfeature/{pageid}', 
	'handler' => ['Controllers\SubscribersController', 'subscriberinfoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/updatefeature', 
	'handler' => ['Controllers\SubscribersController', 'updatesubscriberAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/subscribers/subscriberslist/{pageid}', 
	'handler' => ['Controllers\SubscribersController', 'subscribersdeleteAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/pages/getpageproject', 
	'handler' => ['Controllers\PagesController', 'getPageprojectAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/example/skip/{name}', 
	'handler' => ['Controllers\ExampleController', 'skipAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get',
	'route' => '/user/text/{num}',
	'handler' => ['Controllers\UsersController', 'indexAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/manageusers/{num}/{off}/{keyword}', 
	'handler' => ['Controllers\UserController', 'manageusersAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/deleteuser/{userid}', 
	'handler' => ['Controllers\UserController', 'deleteuserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/edituser/{userid}', 
	'handler' => ['Controllers\UserController', 'edituserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/updateuser', 
	'handler' => ['Controllers\UserController', 'updateuserAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/status/{userid}/{userLevel}', 
	'handler' => ['Controllers\UserController', 'updatestatusAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/editprofile/{userid}', 
	'handler' => ['Controllers\UserController', 'editprofileAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'post', 
	'route' => '/users/updateprofile', 
	'handler' => ['Controllers\UserController', 'updateprofileAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/UploadImage/{filename}', 
	'handler' => ['Controllers\UserController', 'ajaxfileuploaderAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/UploadNewImage/{filename}/{userid}', 
	'handler' => ['Controllers\UserController', 'ajaxfileuploader2Action'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/img', 
	'handler' => ['Controllers\UserController', 'imagelistAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/delimg/{id}', 
	'handler' => ['Controllers\UserController', 'dltphotoAction'],
	'authentication' => FALSE
];

$routes[] = [
	'method' => 'get', 
	'route' => '/users/viewuser/{userid}', 
	'handler' => ['Controllers\UserController', 'viewuserAction'],
	'authentication' => FALSE
];

// hangganan ng rota ni jimmy

return $routes;
