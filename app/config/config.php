<?php

/**
 * Settings to be stored in dependency injector
 */
$settings = array(
    'database' => array(
        'adapter' => 'Mysql', /* Possible Values: Mysql, Postgres, Sqlite */
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'name' => 'dbilchi',
        'port' => 3306
    ),
    'application' => array(
        'baseURL' => 'http://ilchisite/',
        'apiURL' => 'http://ilchiapi/',
    )
);


return $settings;
