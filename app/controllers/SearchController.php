<?php

namespace Controllers;

use PHPMailer as PHPMailer;
use \Models\Pages as Pages;
use \Models\News as News;
use \Models\Video as Video;
use \Models\Album as Album;
use \Models\Newsletter as Newsletter;
use Controllers\ControllerBase as CB;

class SearchController extends \Phalcon\Mvc\Controller {

    public function searchAction($keyword,$offset) {

        // $dc = new CB();
        // $phql =   "select title as title from Models\News WHERE title like '%" .$keyword. "%'";
        // $searchkey = $dc->modelsManager->executeQuery($phql);
    
        // $searchresult = json_encode($searchkey->toArray(), JSON_NUMERIC_CHECK);
        // echo $searchresult;

        $offsetfinal = ($offset * 10) - 10;


        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("
        select * from 
        (
            select title as title,body as body,pageslugs as slugs,type as type from pages
            UNION
            select title as title,body as body,newsslugs as slugs,type as type from news
            UNION
            select title as title,description as body,slugs as slugs,type as type from video
            UNION
            select album_name as title,description as body,album_id as slugs,type as type from album
            UNION
            select title as title,description as body,folderid as slugs,type as type from image
            UNION
            select title as title,body as body,newsletterid as slugs,type as type from newsletter
            UNION
            select feat_title as title,feat_content as body,feat_id as slugs,type as type from featuredprojects
            UNION
            select acttitle as title,actdesc as body,actid as slugs,type as type from calendar
        ) 
            as searchtable Where searchtable.title LIKE '%" . $keyword . "%' or searchtable.body LIKE '%" . $keyword . "' LIMIT " . $offsetfinal . ",10");

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);

    }

    public function pageAction($keyword,$offset) {

      

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("
        select * from 
        (
            select title as title,body as body,pageslugs as slugs,type as type from pages
            UNION
            select title as title,body as body,newsslugs as slugs,type as type from news
            UNION
            select title as title,description as body,slugs as slugs,type as type from video
            UNION
            select album_name as title,description as body,album_id as slugs,type as type from album
            UNION
            select title as title,description as body,folderid as slugs,type as type from image
            UNION
            select title as title,body as body,newsletterid as slugs,type as type from newsletter
            UNION
            select feat_title as title,feat_content as body,feat_id as slugs,type as type from featuredprojects
            UNION
            select acttitle as title,actdesc as body,actid as slugs,type as type from calendar
        ) 
            as searchtable Where searchtable.title LIKE '%" . $keyword . "%' or searchtable.body LIKE '%" . $keyword . "'");

        $stmt->execute();
        $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);

    }

    public function searchautoAction($keyword,$offset) {

        // $dc = new CB();
        // $phql =   "select title as title from Models\News WHERE title like '%" .$keyword. "%'";
        // $searchkey = $dc->modelsManager->executeQuery($phql);
    
        // $searchresult = json_encode($searchkey->toArray(), JSON_NUMERIC_CHECK);
        // echo $searchresult;

        $offsetfinal = ($offset * 10) - 10;


        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("
        select * from 
        (
            select title as title from pages
            UNION
            select title as title from news
            UNION
            select title as title from video
            UNION
            select album_name as title, from album
            UNION
            select title as title from image
            UNION
            select title as title from newsletter
            UNION
            select feat_title as title from featuredprojects
            UNION
            select acttitle as title from calendar
        ) 
            as searchtable");

        if($stmt)
        {
          while($row=mysqli_fetch_array($stmt))
          {
            echo $row['title']."\n";
         }
     }

    }


}
