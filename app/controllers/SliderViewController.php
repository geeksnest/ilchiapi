<?php
namespace Controllers;
use \Models\Sliderimage as Image;
use \Models\Slideralbum  as Album;
class SliderViewController extends \Phalcon\Mvc\Controller
{
    public function slideruploadAction()
    {
        var_dump($_POST);
        
    }
    /*
    * Move File Upload of Programs 
    */
     public function ajaxfileuploaderAction($filename, $folderName,$folderid){
        // $newpicname = 0;
        $data=$filename."/".$folderName."/".$folderid;
        echo json_encode($data);
        // $getType=explode('.', $filename);
        // $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        // $generateid=md5(uniqid(rand(), true));

        // if(is_file('../public/server/php/files/'.$filename)){
        //     rename('../public/server/php/files/'.$filename, '../public/images/'.$folderName.'/'.$newfileName);
        // }
        // if(is_file('../public/server/php/files/thumbnail/'.$filename)){
        //     rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/'.$folderName.'/thumbnail/'.$newfileName);
        // }
       
       
    }
   

  
    public function managealbumAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $album = Album::find();
        } else {
            $conditions = "album_name LIKE '%" . $keyword . "%'";
            $album= Album::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
            "data" => $album,
            "limit" => 15,
            "page" => $currentPage
                )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'album_name' => $m->album_name ,
                'album_id' => $m->album_id
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function albuminfoAction($albumid) {

        $album = Album::findFirst("album_id='" . $albumid."'");
        $album_info = array();
        if ($album) {
            $album_info = array(
                'album_name' => $album ->album_name,
                'album_id' => $album ->album_id,
                'active' => $album ->active
            );
        }
        echo json_encode($album_info);
    }
    public function imagelistAction($getid) {
       $getImage= Image::find(array("folderid='".$getid."'","order" => "sort ASC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'imgid'   => $getImages->id,
                'description'   => $getImages->description,
                'imgpath'          => $getImages->path,
                'imgtitle'          => $getImages->title,
                'foldername'          => $getImages->foldername,
                'sort'          => $getImages->sort
            );
        }
        echo json_encode($data);

     
    }
    

     public function deletealbumAction($albumname) {
        //************************************************************************************************************************************
        //***** DELETE DIRECTORIES INCLUDING THE CONTENT
        function removefolder($album){
            if(is_dir($album)=== true){
                $folderContents = scandir($album);
                unset($folderContents[0],$folderContents[1]);
                foreach ($folderContents as $content => $contentName) {
                    $currentPath=$album.'/'.$contentName;
                    $filetype=filetype($currentPath);
                    if($filetype=='dir'){
                        removefolder($currentPath);
                    }
                    else{
                        unlink($currentPath);
                    }
                    unset($folderContents[$content]);
                }
            }
            rmdir($album);
        }
        removefolder("images\\".$albumname);

        //************************************************************************************************************************************
        //***** DELETE ALBUM
        
        $dltAlbum = Album::find("album_name='".$albumname."'");
        $data = array('error' => 'Not Found');
        if ($dltAlbum) {
            if($dltAlbum->delete()){
              $data = array('success' => 'Album has Been deleted');
            }
        }
        
        //************************************************************************************************************************************
        //***** DELETE IMAGES
        $dltimg = Image::find("foldername='".$albumname."'");
        $data = array('error' => 'Not Found');
        if ($dltimg) {
            if($dltimg->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
         echo json_encode($data);
        
    }
    public function bannertAction() {
        // echo json_encode($id);
        //Count IF 1 EXIST
        $alb = Album::find("active='1'");
        $count=count($alb);
        if($count==0){
            $update = Album::findFirst(array( "active = '0'", "order" => "id DESC", "limit" => 1));
            $set = Album::findFirst('id='.$update->id.'');
            $set->active =1;
            if(!$set->save()){
             $data="error";
            }else{
                $loop = Album::find("active='1'");
                foreach ($loop as $pass){
                $passid= $pass->album_id;
                }
                $testimonial = Image::find(array("folderid='".$passid."'"));
                $msginfo= json_encode($testimonial->toArray(), JSON_NUMERIC_CHECK);
                echo  $msginfo;
            }
        }
        else{
            foreach ($alb as $pass){
                $passid= $pass->album_id;
            }
            $testimonial = Image::find(array("folderid='".$passid."'","order" => "sort ASC"));
            $msginfo= json_encode($testimonial->toArray(), JSON_NUMERIC_CHECK);
            echo  $msginfo;
        }  
   }


   public function editlabumnameAction($id,$name) {
        $album = Album::findfirst("album_id='".$id."'");
        //$count =count($album); //count if data exist
        if(file_exists('../public/images/'.$album->album_name)){
           $data = "File Exist";
           rename('../public/images/'.$album->album_name , '../public/images/'.$name); 
        }else{
           $data = "File Doesn't Exist";
        }
        
        $album->album_name =$name;
         if(!$album->save()){
           $data_1=2;
        }else{
           $data_1=1;
        }

        $upImage= Image::find(array("folderid='".$id."'"));
        foreach ($upImage as $item){
            $updateeach= Image::findfirst(array("id='".$item->id."'"));
            $updateeach->foldername = $name;
            if(!$updateeach->save()){
                $data_2=2;
            }else{
                $data_2=1;
            }
        }
        if($data_1== 1 && $data_2 == 1){
            $data="Data save";
        }else{
            $data="Failed to save Data";
        }
        
        
        echo json_encode($data);
   }




}

