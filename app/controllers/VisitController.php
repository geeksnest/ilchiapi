<?php
namespace Controllers;
use \Models\Visitor as Visitor;
use \Models\News as News;
use \Models\Video as Video;
use \Models\Sliderimage as Sliderimage;
use \Models\Image as Image;
use \Models\System as System;
use \Models\Slideralbum  as Album;

class VisitController extends \Phalcon\Mvc\Controller
{

    //TEST
    // public  $tabledata=array(
    //     "0" =>"ipaddress",
    //     "1" =>"lastvisitdate",
    //     "2" =>"timesvisit"
    //     );
    public function visitAction($ip)
    {

        //TEST DISPLAY
        // $td = new VisitController();
        // var_dump($td);
        ///////////////////////s

        //USED VARIABLES
        $savenewvisitor=0;
        $newvisit=0;
        $note="";
        $dbdate='';
        //Varaible DATA IN TABLE Visitor & Current Date
        $tabledata=array(
            "0" =>"ipaddress",
            "1" =>"lastvisitdate"
            );
        $datetoday=date("Y/m/d");//Current Date

        //////////////////////////////
        //FUNCTION FOR ADD NEW VISITOR
        function newvisitor($ipadd,$tabledata,$date){
            $visit = new Visitor();
            $visit->assign(array(
                ''.$tabledata[0].''  => $ipadd,
                ''.$tabledata[1].''  => $date
                ));

            if (!$visit->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }else {
                $data = array('success' => 'New Visitor has Been save');
            }
            return $data;
        }
        $getvisitor= Visitor::find();
        $countdata= count($getvisitor); 
        foreach ($getvisitor as $data) {
            if($ip==$data->$tabledata[0]){
                $savenewvisitor++;
                $dbdate=$data->$tabledata[1];
            } 
        }
        if ($savenewvisitor==0) {
            newvisitor($ip,$tabledata,$datetoday);
            $note="Save New Data";
        }else{
         $exp_date = $dbdate;
         $todays_date = date("Y-m-d");
         $today = strtotime($todays_date);
         $expiration_date = strtotime($exp_date);

         if ($expiration_date < $today) {
            newvisitor($ip,$tabledata,$datetoday);
            $note="Save New Data";
        }else{
            $note="Nothing to do";
        }

    }
    echo json_encode($note);
    
}
public function countAction()
{
    $count=0;
    $visitors= Visitor::find();
    $count=count($visitors);
   echo json_encode($count);
    

    $alb = Album::find("active='1'");
    $count=count($alb);
    if($count==0){
        $update = Album::findFirst(array( "active = '0'", "order" => "id DESC", "limit" => 1));
        $set = Album::findFirst('id='.$update->id.'');
        $set->active =1;
        if(!$set->save()){
           $data="error";
       }else{
        $data="Save";
    }
}



    
}
public function newsAction()
{

    $news= News::find();
    $count=count($news);
    echo json_encode($count);
}
public function VideoAction()
{
    $Video= Video::find();
    $vid=count($Video);

    $News= News::find(array("category='Video'"));
    $count=count($News) + $vid ;

    
    echo json_encode($count);
}


public function imageAction()
{
    $img= Sliderimage::find();
    $imgcount=count($img);

    $img1= Image::find();
    $imgcount1=count($img1);

    $total= $imgcount + $imgcount1;

    echo json_encode($total);
}

public function imagesizeAction()
{
    function getTotalSize($dir)
    {
        $dir = rtrim(str_replace('\\', '/', $dir), '/');

        if (is_dir($dir) === true) {
            $totalSize = 0;
            $os        = strtoupper(substr(PHP_OS, 0, 3));
        // If on a Unix Host (Linux, Mac OS)
            if ($os !== 'WIN') {
                $io = popen('/usr/bin/du -sb ' . $dir, 'r');
                if ($io !== false) {
                    $totalSize = intval(fgets($io, 80));
                    pclose($io);
                    return $totalSize;
                }
            }
        // If on a Windows Host (WIN32, WINNT, Windows)
            if ($os === 'WIN' && extension_loaded('com_dotnet')) {
                $obj = new \COM('scripting.filesystemobject');
                if (is_object($obj)) {
                    $ref       = $obj->getfolder($dir);
                    $totalSize = $ref->size;
                    $obj       = null;
                    return $totalSize;
                }
            }
        // If System calls did't work, use slower PHP 5
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir));
            foreach ($files as $file) {
                $totalSize += $file->getSize();
            }
            return $totalSize;
        } else if (is_file($dir) === true) {
            return filesize($dir);
        }
    };
    $totalmb= round ((getTotalSize("../public/images")* .0009765625)* .0009765625, 2 );
    echo json_encode($totalmb);
}  

public function diagramAction()
{

    $cmonth = substr(date("m.d.y"),0,2) + 1; 
    $count=0;
    for ($x =1; $x <= 12; $x++){
        $visitors= Visitor::find();
        foreach($visitors as $visit){
            $dbmonth = date('m', strtotime($visit->lastvisitdate)); 
            if($cmonth==$dbmonth){
                $count++;
            }else{$count=0;}
        }
        $data[]=$count;
        if($cmonth>11){
            $cmonth=0;
        }
        $cmonth++;
    }
    echo json_encode($data);   
}   

public function sysperAction()
{

    $tabledata=array(
        "0" =>"memoryusage",
        "1" =>"cpuusage",
        "2" =>"date",
        "3" =>"time"
        );
    $timestamp  =  strftime("%Y-%m-%d %H:%M:%S %Y");
    $time =strftime("%H:%M:%S", strtotime($timestamp))."\n";
    $date =strftime("%Y-%m-%d", strtotime($timestamp))."\n";
    // ///Memmory Usage
    // function get_server_memory_usage(){
    //     $free = shell_exec('free');
    //     $free = (string)trim($free);
    //     $free_arr = explode("\n", $free);
    //     $mem = explode(" ", $free_arr[1]);
    //     $mem = array_filter($mem);
    //     $mem = array_merge($mem);
    //     $memory_usage = $mem[2]/$mem[1]*100;
    //     return $memory_usage;
    // }


    // // //CPU USage
    // function get_server_cpu_usage(){
    //     $load = sys_getloadavg();
    //     return $load[0];
    // }

    // $sysadd = new System();//ADD
    // $sysadd->assign(array(
    //     ''.$tabledata[0].'' => get_server_memory_usage(),
    //     ''.$tabledata[1].'' => get_server_cpu_usage(),
    //     ''.$tabledata[2].'' => $date,
    //     ''.$tabledata[3].'' => $time
    //     ));
    // if (!$sysadd->save()) {
    //     $data['error'] = "Something went wrong saving the data, please try again.";
    // } else {
    //     $data['success'] = "Success";
    // }

    $system= System::find();

    foreach ($system as $data) {
        $data= array(
            'memory'    => round($data->$tabledata[0],2),
            'cpu'       => $data->$tabledata[1],

            );
        $sysdata[]=$data;
    }
    echo json_encode($sysdata);  
}
}

