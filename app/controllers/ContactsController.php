<?php

namespace Controllers;

use \Models\Messages as Messages;


class ContactsController extends \Phalcon\Mvc\Controller {
 public function sendMessageAction(){
     $data = array(); 
     $news = new Messages();
     $news->assign(array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'message' => $_POST['message'],
        'date' => date('Y-m-d'),
        'status' => 0
        ));
     if (!$news->save()) 
     {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } 
    else 
    {
       $data['success'] = "Success";
   }
   echo json_encode($data);
}



}
