<?php

namespace Controllers;
use Models\Userroles as Userroles;
use Models\Roles as Roles;
use Models\Profimg as Profimg;
use Models\Users as Users;
use Models\Forgotpasswords as Forgotpasswords;
use PHPMailer as PHPMailer;
use Controllers\ControllerBase as CB;

class UserController extends \Phalcon\Mvc\Controller {

    public function formRoleAction() {
       $tblroles = Roles::find();
        foreach ($tblroles as $m) {
        $data[] = array(
            'roleCode' => $m->roleCode,
            'roleDescription' => $m->roleDescription
            );
        }

       echo json_encode($data);
    }

    public function listUserAction(){



       $tblroles = Roles::find();
        foreach ($tblroles as $m) {
        $data[] = array(
            'roleCode' => $m->roleCode,
            'roleDescription' => $m->roleDescription
            );
        }

       echo json_encode($data);


       
    }



public function adminLoginAction($username, $password) {
    $data = '';
    $user = Users::findFirst("username='$username'");
    if($user){
        $shapass = sha1($password);
        if($shapass == $user->password){
            $login = true;
            $data = $user; 

            $app = new CB();


            $phql = 'SELECT Tblroles.rolePage,Tblroles.roleDescription, Tblroles.roleCode, Tblroles.roleGroup FROM Models\Userroles as Tbluserroles' .
            ' INNER JOIN Models\Roles as Tblroles ON Tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = '.$user->userid;
            $result = $app->modelsManager->executeQuery($phql);
            $sessrole = array();
            $sesspage = array();

            if($user->userLevel == 1){
                $phql = 'SELECT Tblroles.rolePage, Tblroles.roleCode, Tblroles.roleGroup FROM Models\Roles as Tblroles ';
                $result = $app->modelsManager->executeQuery($phql);
                foreach($result as $r){
                    $sessrole[] = $r->roleCode;
                    $sesspage[$r->roleGroup] = explode(',', str_replace(' ', '', $r->rolePage));
                }
            }else{
                foreach($result as $r){
                    $sessrole[] = $r->roleCode;
                    $sesspage[$r->roleGroup] = explode(',', str_replace(' ', '', $r->rolePage));
                }
            }
            $roles = array('roles' => $sessrole, 'page' => $sesspage);





        }else{
           $login = false;
       }
   }else{
       $login = false;
   }






   echo json_encode(array("login"=>$login, "user"=>$data, "role"=>$roles)); 
}









public function indexAction($id) {

//        $dc = new CB();
//        $phql = "Select * from Models\Members";
//        $query = $dc->modelsManager($phql);
//
//        $data = array();
//        foreach ($query as $r) {
//            $data[] = array(
//                'id' => $r->id,
//                'firstname' => $r->firstname
//            );
//        }

    $dc = new CB();
    var_dump($dc->config->SMTP->host);

    echo json_encode($data);
}

    // ADD USER
public function createSaveAction() {
 var_dump('createSaveAction');
}

    // MANAGE USERS
public function manageusersAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Users::find();
    } else {
        $conditions = "firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%'";
        $Pages = Users::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {

        $data[] = array(
            'userid' => $m->userid,
            'username' => $m->username,
            'email' => $m->email,
            'firstname' => $m->firstname,
            'lastname' => $m->lastname,
            'userLevel' => $m->userLevel
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array(
        'data' => $data, 
        'pages' => $p, 
        'index' => $page->current, 
        'before' => $page->before, 
        'next' => $page->next, 
        'last' => $page->last, 
        'total_items' => $page->total_items
        ));

}

    // DELETE USER
public function deleteuserAction($userid) {
    $user = Users::findFirst(array("userid=".$userid));
    $data = array('error' => 'Not Found');
    if ($user) {
        if($user->delete()){
            $data = array('success' => 'User Deleted');

            $userroles = Userroles::find(array("userID=".$userid));
            if($userroles->delete()){
                $data = array('success' => 'User Roles Deleted');
            }
        }
    }
    echo json_encode($data);
}

    // EDIT USER INFO
public function edituserAction($userid) {

    $user = Users::findFirst("userid=" . $userid);
    $data = array();
    if ($user) {

        $img = "";
        if($user->img == ""){ 
            $img = 'default-page-thumb.png';
        }else{
            if(is_file('../public/images/profile_picture/thumbnail/'.$user->img)){
                $img = $user->img;
            }else{
                $img = 'default-page-thumb.png';
            }
        }

        $expbday = explode(" ", $user->birthday);
        $monts = array('Jan' => 'January', 'Feb' => 'February', 'Mar' => 'March', 'Apr' => 'April', 'May' => 'May', 'Jun' => 'June', 'Jul' => 'July', 'Aug' => 'August', 'Sep' => 'September', 'Oct' => 'October', 'Nov' => 'November', 'Dec' => 'December');

        $data = array(                
            'userid' => $user->userid,
            'username' => $user->username,
            'emailaddress' => $user->email,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'birthday' => $expbday[2]."-".$monts[$expbday[1]]."-".$expbday[3],
            'birthday2' => $expbday[2]."-".$monts[$expbday[1]]."-".$expbday[3],
            'bdaytemp' => $user->birthday,
            'gender' => $user->gender,
            'state' => $user->state,
            'oldpwordtemp' => $user->password,
            'userLevel' => $user->userLevel,
            'imgg' => $img
            );
        $dc = new CB();
        $phql =   ' Select * FROM Models\Userroles WHERE userID = '.$user->userid;
            //Delete if there is existing
        $userroles = $dc->modelsManager->executeQuery($phql);

        $roles = array();
        foreach ($userroles as $ur) {
            $roles[] = array(
                'roleCode' => $ur->userRoles
                );
        }
        $data['userroles'] = $roles;
    }
    echo json_encode($data);
}

    // UPDATE USER
public function UpdateuserAction(){
    $data = array();    
    if ($_POST){ 

        $roles = array();
        $roles['calendarrole'] = $calendarrole = isset($_POST['calendarrole']) ? $_POST['calendarrole'] : "";
        $roles['imagegalleryrole'] =$imagegalleryrole = isset($_POST['imagegalleryrole']) ? $_POST['imagegalleryrole'] : "";
        $roles['newslettersrole'] = $newslettersrole = isset($_POST['newslettersrole']) ? $_POST['newslettersrole'] : "";
        $roles['newsrole'] = $newsrole = isset($_POST['newsrole']) ? $_POST['newsrole'] : "";
        $roles['pagesrole'] = $pagesrole = isset($_POST['pagesrole']) ? $_POST['pagesrole'] : "";
        $roles['projectsrole'] = $projectsrole = isset($_POST['projectsrole']) ? $_POST['projectsrole'] : "";
        $roles['contactsrole'] = $contactsrole = isset($_POST['contactsrole']) ? $_POST['contactsrole'] : "";
        $roles['publicationrole'] = $publicationrole = isset($_POST['publicationrole']) ? $_POST['publicationrole'] : "";
        $roles['slidergalleryrole'] = $slidergalleryrole = isset($_POST['slidergalleryrole']) ? $_POST['slidergalleryrole'] : "";
        $roles['usersrole'] = $usersrole = isset($_POST['usersrole']) ? $_POST['usersrole'] : "";
        $roles['videogalleryrole'] = $videogalleryrole = isset($_POST['videogalleryrole']) ? $_POST['videogalleryrole'] : "";

        if (empty($calendarrole) && empty($imagegalleryrole) && empty($newslettersrole) && empty($newsrole) && empty($pagesrole)
            && empty($projectsrole) && empty($contactsrole) && empty($publicationrole) && empty($slidergalleryrole) && empty($videogalleryrole)  && empty($usersrole)) {
            $data['error'] = "<div class='label label-danger'>At least one role should be selected.</div>";
    } else {

        $monts = array('Jan' => 'January', 'Feb' => 'February', 'Mar' => 'March', 'Apr' => 'April', 'May' => 'May', 'Jun' => 'June', 'Jul' => 'July', 'Aug' => 'August', 'Sep' => 'September', 'Oct' => 'October', 'Nov' => 'November', 'Dec' => 'December');

        $userid = $_POST['userid'];
        $oldpasstemp = $_POST['oldpwordtemp'];
        $oldpasstemp2 = $_POST['oldpwordtemp2'];

        if($oldpasstemp2 != $oldpasstemp){

            $pword = sha1($oldpasstemp2);
            $userpword = Users::findFirst("userid=".$userid." AND password='".$pword."'");

            if($userpword==false){
                ($userpword == false) ? $data["oldpword"] = "Wrong." : '';           
            }else{
                $useruname = Users::findFirst("userid != ".$userid." AND username='".$_POST['username']."'");
                if($useruname == true){
                    ($useruname == true) ? $data["uname"] = "Duplicate." : '';    
                }else{
                    $useremail = Users::findFirst("userid != ".$userid." AND email='".$_POST['emailaddress']."'");
                    if ($useremail == true) {
                        ($useremail == true) ? $data["email"] = "Duplicate." : '';
                    }else{
                        $user = Users::findFirst('userid='.$userid.' ');
                        $user->username = $_POST['username'];
                        $user->email = $_POST['emailaddress'];
                        $user->password = sha1($_POST['oldpasstemp2']);
                        $user->firstname = $_POST['firstname'];
                        $user->lastname = $_POST['lastname'];
                        $user->birthday = $_POST['bdaytemp2'];
                        $user->gender = $_POST['gender'];
                        $user->state = $_POST['state'];

                        if (!$user->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                        } else {
                            $data['success'] = "Success";

                            $dc = new CB();
                                    //Delete if there is existing
                            $dc->modelsManager("DELETE FROM Models\Userroles WHERE userID = ".$userid);

                                    // Inserting using placeholders
                            $phql = "INSERT INTO Models\Userroles ( userID, userRoles) "
                            . "VALUES (:userid:, :role:)";
                            foreach ($roles as $r => $v) {
                                if ($v == "true") {
                                    $dc->modelsManager->executeQuery($phql, array(
                                        'userid' => $userid,
                                        'role' => $r,
                                        ));
                                }
                            }
                        }
                    }    
                }     
            }
        }else{
            $useruname = Users::findFirst("userid != ".$userid." AND username='".$_POST['username']."'");
            if($useruname == true){
                ($useruname == true) ? $data["uname"] = "Duplicate." : '';    
            }else{
                $useremail = Users::findFirst("userid != ".$userid." AND email='".$_POST['emailaddress']."'");
                if ($useremail == true) {
                    ($useremail == true) ? $data["email"] = "Duplicate." : '';
                }else{
                    $user = Users::findFirst('userid='.$userid.' ');
                    $user->username = $_POST['username'];
                    $user->email = $_POST['emailaddress'];
                    $user->firstname = $_POST['firstname'];
                    $user->lastname = $_POST['lastname'];
                    $user->birthday = $_POST['bdaytemp2'];
                    $user->gender = $_POST['gender'];
                    $user->state = $_POST['state'];

                    if (!$user->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";

                        $dc = new CB();
                                    //Delete if there is existing
                        $dc->modelsManager("DELETE FROM Models\Userroles WHERE userID = ".$userid);

                                    // Inserting using placeholders
                        $phql = "INSERT INTO Models\Userroles ( userID, userRoles) "
                        . "VALUES (:userid:, :role:)";
                        foreach ($roles as $r => $v) {
                            if ($v == "true") {
                                $dc->modelsManager->executeQuery($phql, array(
                                    'userid' => $userid,
                                    'role' => $r,
                                    ));
                            }
                        }
                    }
                }    
            }
        }                                
    }
}
echo json_encode($data);
}

    // EDIT PROFILE INFO
public function editprofileAction($userid) {

    $profile = Users::findFirst("userid=" . $userid);
    $data = array();
    if ($profile) {

        $img = "";
        if($profile->img == ""){ 
            $img = 'default-page-thumb.png';
        }else{
            if(is_file('../public/images/profile_picture/thumbnail/'.$profile->img)){
                $img = $profile->img;
            }else{
                $img = 'default-page-thumb.png';
            }
        }

        $expbday = explode(" ", $profile->birthday);
        $monts = array('Jan' => 'January', 'Feb' => 'February', 'Mar' => 'March', 'Apr' => 'April', 'May' => 'May', 'Jun' => 'June', 'Jul' => 'July', 'Aug' => 'August', 'Sep' => 'September', 'Oct' => 'October', 'Nov' => 'November', 'Dec' => 'December');

        $data = array(
            'userid' => $profile->userid,
            'username' => $profile->username,
            'emailaddress' => $profile->email,
            'firstname' => $profile->firstname,
            'lastname' => $profile->lastname,
            'birthday' => $expbday[2]."-".$monts[$expbday[1]]."-".$expbday[3],
            'birthday2' => $expbday[2]."-".$monts[$expbday[1]]."-".$expbday[3],
            'bdaytemp' => $profile->birthday,
            'gender' => $profile->gender,
            'state' => $profile->state,
            'oldpwordtemp' => $profile->password,
            'userLevel' => $profile->userLevel,
            'imgg' => $img
            );
    }
    echo json_encode($data);
}

    // UPDATE PROFILE
public function updateprofileAction(){
    $data = array();    
    if ($_POST){                         

        $userid = $_POST['userid'];
        $oldpasstemp = $_POST['oldpwordtemp'];
        $oldpasstemp2 = $_POST['oldpwordtemp2'];

        if($oldpasstemp2 != $oldpasstemp){

            $pword = sha1($oldpasstemp2);
            $userpword = Users::findFirst("userid=".$userid." AND password='".$pword."'");

            if($userpword==false){
                ($userpword == false) ? $data["oldpword"] = "Wrong." : '';           
            }else{
                $useruname = Users::findFirst("userid != ".$userid." AND username='".$_POST['username']."'");
                if($useruname == true){
                    ($useruname == true) ? $data["uname"] = "Duplicate." : '';    
                }else{
                    $useremail = Users::findFirst("userid != ".$userid." AND email='".$_POST['emailaddress']."'");
                    if ($useremail == true) {
                        ($useremail == true) ? $data["email"] = "Duplicate." : '';
                    }else{
                        $user = Users::findFirst('userid='.$userid);
                        $user->username = $_POST['username'];
                        $user->email = $_POST['emailaddress'];
                        $user->password = sha1($_POST['password']);
                        $user->firstname = $_POST['firstname'];
                        $user->lastname = $_POST['lastname'];
                        $user->birthday = $_POST['bdaytemp2'];
                        $user->gender = $_POST['gender'];
                        $user->state = $_POST['state'];

                        if (!$user->save()) {
                            $data['error'] = "Error";
                        } else {
                            $data['success'] = "Success";
                        }
                    }    
                }     
            }

        }else{

            $useruname = Users::findFirst("userid != ".$userid." AND username='".$_POST['username']."'");
            if($useruname == true){
                ($useruname == true) ? $data["uname"] = "Duplicate." : '';    
            }else{
                $useremail = Users::findFirst("userid != ".$userid." AND email='".$_POST['emailaddress']."'");
                if ($useremail == true) {
                    ($useremail == true) ? $data["email"] = "Duplicate." : '';
                }else{
                    $user = Users::findFirst('userid='.$userid);
                    $user->username = $_POST['username'];
                    $user->email = $_POST['emailaddress'];
                    $user->firstname = $_POST['firstname'];
                    $user->lastname = $_POST['lastname'];
                    $user->birthday = $_POST['bdaytemp2'];
                    $user->gender = $_POST['gender'];
                    $user->state = $_POST['state'];

                    if (!$user->save()) {
                        $data['error'] = "Error2";
                    } else {
                        $data['success'] = "Success";
                    }
                }    
            }
        }                                
    }

    echo json_encode($data);
}

    // UPDATE STATUS
public function updatestatusAction($userid,$userLevel) {
    $user = Users::findFirst('userid='.$userid.'');

    if($userLevel==3){
        $userLevel=2;
    }elseif($userLevel==2) {
        $userLevel=3;
    }
    $user->userLevel = $userLevel;
    if(!$user->save()){
     $data="error";
 }else{
    $data="Success";
}
echo json_encode($data);
}

public function ajaxfileuploaderAction($filename){
    $newpicname = 0;
    $limit_size=250000;
    $getType=explode('.', $filename);
        $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        $generateid=md5(uniqid(rand(), true));

        if(is_file('../public/server/php/files/'.$filename)){ 
            unlink('../public/server/php/files/'.$filename);           
        }

        if(is_file('../public/server/php/files/thumbnail/'.$filename)){
            rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/profile_picture/thumbnail/'.$newfileName);         
        }

        // start
        $dlttemp = Profimg::find();
        if ($dlttemp) {
            if($dlttemp->delete()){
                // $data = array('success' => 'Photo has Been deleted');
            }
        }
        $imgUpload = new Profimg();
        $imgUpload->assign(array('img' => $newfileName));
        if (!$imgUpload->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
            echo json_encode(["error" => $imgUpload->getMessages()]);
        }else{
            $getImage= Profimg::find(array("order" => "id DESC"));
            foreach ($getImage as $getImages) {
                $data[] = array('img' => $getImages->img );
            }
            echo json_encode($data);
        }
        // end
    }

    public function ajaxfileuploader2Action($filename, $userid){
        $newpicname = 0;

        $getType=explode('.', $filename);
        $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        $generateid=md5(uniqid(rand(), true));

        if(is_file('../public/server/php/files/'.$filename)){ 
            unlink('../public/server/php/files/'.$filename);           
        }

        if(is_file('../public/server/php/files/thumbnail/'.$filename)){
            rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/profile_picture/thumbnail/'.$newfileName);
        }

        $dlttemp = Profimg::find();
        if ($dlttemp) {
            if($dlttemp->delete()){
                // $data = array('success' => 'Photo has Been deleted');
            }
        }
        $imgUpload = new Profimg();
        $imgUpload->assign(array(
            'img' => $newfileName
            ));
        if (!$imgUpload->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
            echo json_encode(["error" => $imgUpload->getMessages()]);
        }else{
            $user = Users::findFirst('userid='.$userid);
            $user->img = $newfileName;
            if (!$user->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";                
            }
            $getImage= Profimg::find(array("order" => "id DESC"));
            foreach ($getImage as $getImages) {
                $data[] = array(
                    'img' => $getImages->img
                    );
            }
            echo json_encode($data);
        } 
    }

    /*
    * Display Content
    */
    public function imagelistAction(){

        $getImage= Profimg::find();
        $count=count($getImage);
        if($count==0){
            $images[]=array(
                'img'  => "default-page-thumb.png",
                );
        }else{
         foreach ($getImage as $getImages) {
            $images[] = array(
                'id'   => $getImages->id,
                'img' => $getImages->img
                );
        }
    }

    echo json_encode($images);
}

public function dltphotoAction($id){
    $dltPhoto = Profimg::findFirst('id='.$id.' ');
    $data = array('error' => 'Not Found');
    if ($dltPhoto) {
        if($dltPhoto->delete()){
            $data = array('success' => 'Photo has Been deleted');
        }
    }
    echo json_encode($data);
}

public function viewuserAction($userid) {

    $profile = Users::findFirst("userid=" . $userid);
    $data = array();
    if ($profile) {

        $img = "";
        if($profile->img == ""){ 
            $img = 'default-page-thumb.png';
        }else{

            if(is_file('../public/images/profile_picture/thumbnail/'.$profile->img)){
                $img = $profile->img;
            }else{
                $img = 'default-page-thumb.png';
            }
        }

        $data = array(
            'userid' => $profile->userid,
            'username' => $profile->username,
            'emailaddress' => $profile->email,
            'firstname' => $profile->firstname,
            'lastname' => $profile->lastname,
            'userLevel' => $profile->userLevel,
            'imgg' => $img
            );
    }
    echo json_encode($data);
}

public function forgotpasswordAction($email) {
   $data = array();
   $NMSemail = $email;


   $SubsEmail = Users::findFirst("email='" . $NMSemail . "'");
   if ($SubsEmail == true) 
   {




    $a = '';
    for ($i = 0; $i < 6; $i++) {
        $a .= mt_rand(0, 9);
    }
    $token = sha1($a);



    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->Host = 'smtp.mandrillapp.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'efrenbautistajr@gmail.com';
    $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
    //$mail->SMTPSecure = 'ssl';                    
    $mail->Port = 587;
    $mail->From = 'no-reply@ilchi.com';
    $mail->FromName = 'Ilchi Password Reset';
    $mail->addAddress($NMSemail, 'somename');
    $mail->isHTML(true);
    $mail->Subject = 'Password Reset';
    $mail->Body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!--[if gte mso 9]>
    <style>
      .column-top {
        mso-line-height-rule: exactly !important;
    }
</style>
<![endif]-->
<meta name="robots" content="noindex,nofollow" />
<meta property="og:title" content="My First Campaign" />
<link rel="stylesheet" type="text/css" href="http://www.ilchi.com/contact-us/">
</head>
<body style="margin: 0;mso-line-height-rule: exactly;padding: 0;min-width: 70%;background-color: #fbfbfb"><style type="text/css">
    body,.wrapper,.emb-editor-canvas{background-color:#fbfbfb}.border{background-color:#e9e9e9}h1{color:#565656}.wrapper h1{}.wrapper h1{font-family:sans-serif}@media only screen and (min-width: 0){.wrapper h1{font-family:Avenir,sans-serif !important}}h1{}.one-col h1{line-height:42px}.two-col h1{line-height:32px}.three-col h1{line-height:26px}.wrapper .one-col-feature h1{line-height:58px}@media only screen and (max-width: 620px){h1{line-height:42px !important}}h2{color:#555}.wrapper h2{}.wrapper h2{font-family:Georgia,serif}h2{}.one-col h2{line-height:32px}.two-col h2{line-height:26px}.three-col h2{line-height:22px}.wrapper .one-col-feature h2{line-height:52px}@media only screen and (max-width: 620px){h2{line-height:32px !important}}h3{color:#555}.wrapper h3{}.wrapper h3{font-family:Georgia,serif}h3{}.one-col h3{line-height:26px}.two-col h3{line-height:22px}.three-col 
    h3{line-height:20px}.wrapper .one-col-feature h3{line-height:42px}@media only screen and (max-width: 620px){h3{line-height:26px !important}}p,ol,ul{color:#565656}.wrapper p,.wrapper ol,.wrapper ul{}.wrapper p,.wrapper ol,.wrapper ul{font-family:Georgia,serif}p,ol,ul{}.one-col p,.one-col ol,.one-col ul{line-height:25px;Margin-bottom:25px}.two-col p,.two-col ol,.two-col ul{line-height:23px;Margin-bottom:23px}.three-col p,.three-col ol,.three-col ul{line-height:21px;Margin-bottom:21px}.wrapper .one-col-feature p,.wrapper .one-col-feature ol,.wrapper .one-col-feature ul{line-height:32px}.one-col-feature blockquote p,.one-col-feature blockquote ol,.one-col-feature blockquote ul{line-height:50px}@media only screen and (max-width: 620px){p,ol,ul{line-height:25px !important;Margin-bottom:25px !important}}.image{color:#565656}.image{font-family:Georgia,serif}.wrapper a{color:#41637e}.wrapper 
    a:hover{color:#30495c !important}.wrapper .logo div{color:#41637e}.wrapper .logo div{font-family:sans-serif}@media only screen and (min-width: 0){.wrapper .logo div{font-family:Avenir,sans-serif !important}}.wrapper .logo div a{color:#41637e}.wrapper .logo div a:hover{color:#41637e !important}.wrapper .one-col-feature p a,.wrapper .one-col-feature ol a,.wrapper .one-col-feature ul a{border-bottom:1px solid #41637e}.wrapper .one-col-feature p a:hover,.wrapper .one-col-feature ol a:hover,.wrapper .one-col-feature ul a:hover{color:#30495c !important;border-bottom:1px solid #30495c !important}.btn a{}.wrapper .btn a{}.wrapper .btn a{font-family:Georgia,serif}.wrapper .btn a{background-color:#41637e;color:#fff !important;outline-color:#41637e;text-shadow:0 1px 0 #3b5971}.wrapper .btn a:hover{background-color:#3b5971 !important;color:#fff !important;outline-color:#3b5971 !important}.preheader 
    .title,.preheader .webversion,.footer .padded{color:#999}.preheader .title,.preheader .webversion,.footer .padded{font-family:Georgia,serif}.preheader .title a,.preheader .webversion a,.footer .padded a{color:#999}.preheader .title a:hover,.preheader .webversion a:hover,.footer .padded a:hover{color:#737373 !important}.footer .social .divider{color:#e9e9e9}.footer .social .social-text,.footer .social a{color:#999}.wrapper .footer .social .social-text,.wrapper .footer .social a{}.wrapper .footer .social .social-text,.wrapper .footer .social a{font-family:Georgia,serif}.footer .social .social-text,.footer .social a{}.footer .social .social-text,.footer .social a{letter-spacing:0.05em}.footer .social .social-text:hover,.footer .social a:hover{color:#737373 !important}.image .border{background-color:#c8c8c8}.image-frame{background-color:#dadada}.image-background{background-color:#f7f7f7}
</style>
<center class="wrapper" style="display: table;table-layout: fixed;width: 70%;min-width: 620px;-webkit-text-size-adjust: 70%;-ms-text-size-adjust: 70%;background-color: #fbfbfb">
    <table class="gmail" style="border-collapse: collapse;border-spacing: 0;width: 650px;min-width: 650px"><tbody><tr><td style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px">&nbsp;</td></tr></tbody></table>
    <table class="preheader centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto">
      <tbody><tr>
        <td style="padding: 0;vertical-align: top">
          <table style="border-collapse: collapse;border-spacing: 0;width: 602px">
            <tbody><tr>
              <td class="title" style="padding: 0;vertical-align: top;padding-top: 10px;padding-bottom: 12px;font-size: 12px;line-height: 21px;text-align: left;color: #999;font-family: Georgia,serif">info@ilchi.com</td>            
          </tr>
      </tbody></table>
  </td>
</tr>
</tbody></table>
<table class="header centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 602px">
  <tbody><tr><td class="border" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e9e9e9;width: 1px">&nbsp;</td></tr>
    <tr><td class="logo" style="padding: 32px 0;vertical-align: top;mso-line-height-rule: at-least"><div class="logo-left" style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif" align="left" id="emb-email-header"><img style="border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 134px" src="'.$GLOBALS["baseURL"].'/images/headerlogo.png" alt="" width="134" height="42" /></div></td></tr>
</tbody></table>    
<table class="border" style="border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #e9e9e9;Margin-left: auto;Margin-right: auto" width="602">
    <tbody><tr><td style="padding: 0;vertical-align: top">&#8203;</td></tr>
    </tbody></table>      
    <table class="centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto">
      <tbody><tr>
        <td class="border" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e9e9e9;width: 1px">&#8203;</td>
        <td style="padding: 0;vertical-align: top">
          <table class="one-col gray" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;background-color: #f7f7f7;font-size: 14px">
            <tbody><tr>
              <td class="column" style="padding: 0;vertical-align: top;text-align: left">
                <div><div class="column-top" style="font-size: 32px;line-height: 32px">&nbsp;</div></div>
                <table class="contents" style="border-collapse: collapse;border-spacing: 0;width: 100%">
                  <tbody><tr>
                    <td class="padded" style="padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px">                        


                        You&#39;re receiving this e-mail because you requested a password reset for your account at ilchi.com.
                        <br>
                        <br>
                        Please go to the following link and choose a new password:
                        <br>
                        '.$GLOBALS["baseURL"].'ilchiadmin/forgotpassword/changepassword/'.$NMSemail.'/'.$token.'
                        <br>
                        <br>
                        <br>  
                        Thanks for using Ilchi Board!


                    </td>
                </tr>
            </tbody></table>                  
            <div class="column-bottom" style="font-size: 8px;line-height: 8px">&nbsp;</div>
        </td>
    </tr>
</tbody></table>
</td>
<td class="border" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e9e9e9;width: 1px">&#8203;</td>
</tr>
</tbody></table>      
<table class="border" style="border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #e9e9e9;Margin-left: auto;Margin-right: auto" width="602">
  <tbody><tr class="border" style="font-size: 1px;line-height: 1px;background-color: #e9e9e9;height: 1px">
    <td class="border" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e9e9e9;width: 1px">&#8203;</td>
    <td style="padding: 0;vertical-align: top;line-height: 1px">&#8203;</td>
    <td class="border" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e9e9e9;width: 1px">&#8203;</td>
</tr>
</tbody></table>                  
<div class="spacer" style="font-size: 1px;line-height: 32px;width: 100%">&nbsp;</div>
<table class="footer centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 602px">
  <tbody><tr>
    <td class="social" style="padding: 0;vertical-align: top;padding-top: 32px;padding-bottom: 22px" align="center">
      <table style="border-collapse: collapse;border-spacing: 0">
        <tbody><tr>
          <td class="social-link" style="padding: 0;vertical-align: top">
            <table style="border-collapse: collapse;border-spacing: 0">
              <tbody><tr>
                <td style="padding: 0;vertical-align: top">
                  <fblike style="text-decoration:none;">
                    <img style="border: 0;-ms-interpolation-mode: bicubic;display: block" src="https://i5.createsend1.com/static/eb/master/01-mason/images/facebook-dark.png" width="26" height="21" />
                </fblike>
            </td>
            <td class="social-text" style="padding: 0;vertical-align: middle !important;height: 21px;font-size: 10px;font-weight: bold;text-decoration: none;text-transform: uppercase;color: #999;letter-spacing: 0.05em;font-family: Georgia,serif">
              <fblike style="text-decoration:none;">
                <a href="https://www.facebook.com/">Like</a>
            </fblike>
        </td>
    </tr>
</tbody></table>
</td>
<td class="divider" style="padding: 0;vertical-align: top;font-family: sans-serif;font-size: 10px;line-height: 21px;text-align: center;padding-left: 14px;padding-right: 14px;color: #e9e9e9">
    <img style="border: 0;-ms-interpolation-mode: bicubic;display: block" src="https://i6.createsend1.com/static/eb/master/01-mason/images/diamond.png" width="5" height="21" alt="" />
</td>
<td class="social-link" style="padding: 0;vertical-align: top">
    <table style="border-collapse: collapse;border-spacing: 0">
      <tbody><tr>
        <td style="padding: 0;vertical-align: top">
          <tweet style="text-decoration:none;">
            <img style="border: 0;-ms-interpolation-mode: bicubic;display: block" src="https://i7.createsend1.com/static/eb/master/01-mason/images/twitter-dark.png" width="26" height="21" />
        </tweet>
    </td>
    <td class="social-text" style="padding: 0;vertical-align: middle !important;height: 21px;font-size: 10px;font-weight: bold;text-decoration: none;text-transform: uppercase;color: #999;letter-spacing: 0.05em;font-family: Georgia,serif">
      <tweet style="text-decoration:none;">
        <a href="https://twitter.com/">Tweet</a>
    </tweet>
</td>
</tr>
</tbody></table>
</td>                
<td class="social-link" style="padding: 0;vertical-align: top">                  
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr><td class="border" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e9e9e9;width: 1px">&nbsp;</td></tr>
<tr>
    <td style="padding: 0;vertical-align: top">
      <table style="border-collapse: collapse;border-spacing: 0">
        <tbody><tr>
          <td class="address" style="padding: 0;vertical-align: top;width: 250px;padding-top: 32px;padding-bottom: 64px">
            <table class="contents" style="border-collapse: collapse;border-spacing: 0;width: 100%">
              <tbody><tr>
                <td class="padded" style="padding: 0;vertical-align: top;padding-left: 0;padding-right: 10px;text-align: left;font-size: 12px;line-height: 20px;color: #999;font-family: Georgia,serif">
                  <div>CONTACT US<br />
                    Ilchi<br />
                    Their address<br />
                    <br />
                    Phone : 900-000-0000<br />
                    Email: Ilchi@email</div>
                </td>
            </tr>
        </tbody></table>
    </td>
    <td class="subscription" style="padding: 0;vertical-align: top;width: 350px;padding-top: 32px;padding-bottom: 64px">
      <table class="contents" style="border-collapse: collapse;border-spacing: 0;width: 100%">
        <tbody><tr>
          <td class="padded" style="padding: 0;vertical-align: top;padding-left: 10px;padding-right: 0;font-size: 12px;line-height: 20px;color: #999;font-family: Georgia,serif;text-align: right">                          
            <div>
              <span class="block">                              
              </span>                      
          </div>
      </td>
  </tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</center>
</body></html>
';

if (!$mail->send()) {
    $data = array('error' => $mail->ErrorInfo);
}
else 
{

    $forgotEmail = Forgotpasswords::findFirst("email='" . $NMSemail . "'");
    if ($forgotEmail == true) 
    {

        if ($forgotEmail->delete()) {

        }

    }



    $forgotpassword = new Forgotpasswords();
    $forgotpassword->assign(array(
        'email' => $NMSemail,
        'token' => $token,
        'date' => date('m-d-Y')
        ));
    if (!$forgotpassword->save()) 
    {
        $data['msg'] = "Something went wrong saving the data, please try again.";

    } 
    else 
    {


        $data['msg'] = 'Password Reset has been sent to your Email '. $NMSemail;
    }




}



}

else
{
    $data = array('msg' => 'No account found with that email address.');

}
echo json_encode($data);
}

public function checktokenAction($token) {
   $data = array();
   $forgottoken = Forgotpasswords::findFirst("token='" . $token . "'");
   if ($forgottoken == true) 
   {

    $data['msg'] = 'valid';

}
else
{
    $data['msg'] = 'invalid';
}
echo json_encode($data);
}

public function updatepasswordtokenAction() {

    $email = $_POST['email'];
    $token = $_POST['token'];
    $repassword = $_POST['repassword'];

    $data = array();
    $emailcheck = Users::findFirst("email='" . $email . "'");
    if ($emailcheck == true) 
    {



        $emailcheck->password = sha1($_POST['repassword']);


        if (!$emailcheck->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else 
        {

         $forgotEmail = Forgotpasswords::findFirst("token='" . $token . "'");
         if ($forgotEmail == true) 
         {

            if ($forgotEmail->delete()) {

            }

        }
        $data['msg'] = "Password Change Success.";

    }

}
else
{

}
echo json_encode($data);

}


}
