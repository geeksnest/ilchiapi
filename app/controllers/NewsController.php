<?php

namespace Controllers;

use \Models\News as News;
use \Models\Newscategory as Newscategory;
use \Models\Tags as Tags;
use \Models\Category as Category;


class NewsController extends \Phalcon\Mvc\Controller {


    public function newsSlugAction($slug) {
      $news = News::find("newsslugs = '". $slug ."'");
      (count($news) == 0? $data = true : $data = false); 
      echo json_encode($data);
  }




  public function createNewsAction()
  {

   $data = array(); 

   $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
   $date = explode(" ", $_POST['date']);
   $d = $date[3].'-'.$mont0[$date[1]].'-'.$date[2];

   if ($_POST)
   {
    $status = 0;

    if($_POST['check'])
    {
        $status = 1;
    }
    else
    {
       $status = 0;
   }

   $news = new News();
   $news->assign(array(
    'title' => $_POST['title'],
    'newsslugs' => $_POST['slugs'],
    'author' => $_POST['author'],
    'body' => $_POST['body'],
    'banner' => $_POST['banner'],
    'category' => $_POST['category'],
    'status' => $status,
    'date' => $d,
    'views'=> 0,
    'type'=> $_POST['category'],
    'metatitle' => $_POST['metatitle'],
    'metadesc' => $_POST['metadesc'],
    'metakeyword' => $_POST['keyword']

    ));
   if (!$news->save()) 
   {
    $data['error'] = "Something went wrong saving the data, please try again.";

} 
else 
{

    $newsid = $news->newsid;
    $tags = array(); 
    $tags = $_POST['tags'];
    foreach($tags as $tag){
        $newscategory = new Newscategory();
        $newscategory->assign(array(
            'newsid' => $newsid,
            'categoryid' => $tag 
            ));
        if (!$newscategory->save()) 
        {
            $data['error'] = "Something went wrong saving the data, please try again.";

        } 
        else 
        {
           $data['success'] = "Success";
       }
   }




   $data['success'] = "Success";
}
}
echo json_encode($data);




}


public function managenewsAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = News::find();
    } else {
        $conditions = "title LIKE '%" . $keyword . "%'";
        $Pages = News::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'newsid' => $m->newsid,
            'title' => $m->title,
            'status' => $m->status
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}


public function managetagsAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Tags::find();
    } else {
        $conditions = "categoryname LIKE '%" . $keyword . "%'";
        $Pages = Tags::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'id' => $m->id,
            'categoryname' => $m->categoryname,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}



public function listcategoryAction()
{

    $getcategory= Tags::find(array("order" => "id ASC"));
    foreach ($getcategory as $getcategory) {
        $data[] = array(
            'id'=>$getcategory->id,
            'categoryname'=>$getcategory->categoryname
            );
    }
    echo json_encode($data);

}

public function newsdeleteAction($newsid) {
    $conditions = "newsid=" . $newsid;
    $news = News::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'News Deleted');
        }
    }
    echo json_encode($data);
}


public function tagsdeleteAction($id) {
    $conditions = "id=" . $id;
    $news = Tags::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'Category Deleted');
        }
    }
    echo json_encode($data);
}


public function newsinfoAction($newsid) {

    $news = News::findFirst("newsid=" . $newsid);
    $data = array();
    $catdata = array();
    if ($news) {
        $getcategory= Newscategory::find("newsid=" . $newsid);
        foreach ($getcategory as $getcategory) {;
            $catdata[] = array(
                'newsid'=>$getcategory->newsid,
                'categoryid'=>$getcategory->categoryid
                );
        }
        $data = array(
            'newsid' => $news->newsid,
            'title' => $news->title,
            'author' => $news->author,
            'slugs' => $news->newsslugs,
            'body' => $news->body,
            'banner' => $news->banner,
            'category' => $news->category,
            'check' => $news->status,
            'date' => $news->date,
            'date2' => $news->date,
            'catdata' => $catdata,
            'metatitle' => $news->metatitle,
            'metadesc' => $news->metadesc,
            'keyword' => $news->metakeyword,
            );
    }

    echo json_encode($data);

}




public function newsUpdateAction() 
{

    $data = array();


    $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
    $date = explode(" ", $_POST['date']);
    $d = $date[3].'-'.$mont0[$date[1]].'-'.$date[2];

    if ($_POST) {


        if ($_POST['check'] == "0") {
            $status = 0;
        } else {
            $status = 1;
        }

        $newsid = $_POST['newsid'];
        $news = News::findFirst('newsid=' . $newsid . ' ');
        $news->title = $_POST['title'];
        $news->newsslugs = $_POST['slugs'];
        $news->author = $_POST['author'];
        $news->body = $_POST['body'];
        $news->banner = $_POST['banner'];
        $news->category =  $_POST['category'];
        $news->status = $status;
        $news->date = $d;
        $news->type =  $_POST['category'];
        $news->metatitle   =$_POST['metatitle'];
        $news->metadesc    =$_POST['metadesc'];
        $news->metakeyword =$_POST['keyword']; 

        if (!$news->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {

         $conditions = "newsid=" . $newsid;
         $newscat = Newscategory::find(array($conditions));
         $data = array('error' => 'Not Found');
         if ($newscat) {
            if ($newscat->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }



        $tags = array(); 
        $tags = $_POST['tags'];
        foreach($tags as $tag){
            $newscategory = new Newscategory();
            $newscategory->assign(array(
                'newsid' => $newsid,
                'categoryid' => $tag 
                ));
            if (!$newscategory->save()) 
            {
                $data['error'] = "Something went wrong saving the data, please try again.";

            } 
            else 
            {
               $data['success'] = "Success";
           }

       }

       $data['success'] = "Success";
   }
}
echo json_encode($data);
}


public function countUpdateAction($newsslugs) 
{

    $data = array();



    $news = News::findFirst('newsslugs="' . $newsslugs . '"');
    $news->views = $news->views + 1;

    if (!$news->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else
    {

        $data['success'] = "Success";
    }
    echo json_encode($data);
}


public function tagsUpdateAction($catname,$id) 
{

    $data = array();
    $news = Tags::findFirst('id=' . $id . ' ');
    $news->categoryname = $catname;

    if (!$news->save()) 
    {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } 
    else 
    {
        $data['success'] = "Success";
    }
    echo json_encode($data);
}




public function createtagsAction()
{

 $data = array(); 

 $catnames = new Tags();
 $catnames->assign(array(
    'categoryname' => $_POST['catnames'],
    ));
 if (!$catnames->save()) 
 {
    $data['error'] = "Something went wrong saving the data, please try again.";
} 
else 
{
   $data['success'] = "Success";
}
echo json_encode($data);
}

public function showPostAction($offset) {

    $news = News::find(array("status=1","order"=>"date DESC", "limit" => array("number" => 5, "offset" => $offset)));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}

public function popularpostAction($offset) {

    $news = News::find(array("status=1","order"=>"views DESC", "limit" => array("number" => 5, "offset" => $offset)));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}
public function showLatestPostAction($offset) {

    $news = News::find(array("category='News' and status=1","order"=>"date DESC", "limit" => array("number" => 1, "offset" => $offset)));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}

public function showJournalPostAction($offset) {

    $news = News::find(array("category='Journal' and status=1","order"=>"date DESC", "limit" => array("number" => 1, "offset" => $offset)));
    $postjournal = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postjournal;
}


public function showNewsAction($offset,$category) {

    $news = News::find(array("status=1 and category='".$category."'","order"=>"date DESC", "limit" => array("number" => 5, "offset" => $offset)));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}

public function showNewsrelatedAction($offset,$category,$newsslugs) {

    $news = News::find(array("status=1 and newsslugs!='".$newsslugs."' and category='".$category."'","order"=>"date DESC", "limit" => array("number" => 5, "offset" => $offset)));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}



public function fullnewsAction($newsslugs) {

    $news = News::findFirst("newsslugs='" . $newsslugs."'");
    $data = array();
    $catdata = array();
    if ($news) {
        $getcategory= Newscategory::find("newsid=" . $news->newsid);
        foreach ($getcategory as $getcategory) {

            if($getcategoryname= Tags::findfirst("id=" . $getcategory->categoryid))
            {
                $catdata[] = array(
                    'newsid'=>$getcategory->newsid,
                    'id'=>$getcategoryname->id,
                    'categoryname'=>$getcategoryname->categoryname
                    );
            }

        }
        $data = array(
            'newsid' => $news->newsid,
            'title' => $news->title,
            'newsslugs' => $news->newsslugs,
            'author' => $news->author,
            'body' => $news->body,
            'banner' => $news->banner,
            'category' => $news->category,
            'check' => $news->status,
            'date' => $news->date,
            'catdata' => $catdata,
            'metatitle' => $news->metatitle,
            'metadesc' => $news->metadesc,
            'keyword' => $news->metakeyword
            );
    }

    echo json_encode($data);
}



public function managecategoryAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Category::find();
    } else {
        $conditions = "categoryname LIKE '%" . $keyword . "%'";
        $Pages = Category::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'id' => $m->id,
            'categoryname' => $m->categoryname,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}


public function categorydeleteAction($id) {
    $conditions = "id=" . $id;
    $news = Category::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'Category Deleted');
        }
    }
    echo json_encode($data);
}



public function updatecategoryAction($catname,$id) 
{

    $data = array();
    $news = Category::findFirst('id=' . $id . ' ');
    $news->categoryname = $catname;

    if (!$news->save()) 
    {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } 
    else 
    {
        $data['success'] = "Success";
    }
    echo json_encode($data);
}

public function categorylistAction(){

    $getlist = Category::find(array("order" => "id DESC"));
    foreach ($getlist as $getlist) {;
        $data[] = array(
            'categoryname'=>$getlist->categoryname
            );
    }
    echo json_encode($data);
}    


public function createcategoryAction()
{

 $data = array(); 

 $catnames = new Category();
 $catnames->assign(array(
    'categoryname' => $_POST['catnames'],
    ));
 if (!$catnames->save()) 
 {
    $data['error'] = "Something went wrong saving the data, please try again.";
} 
else 
{
   $data['success'] = "Success";
}
echo json_encode($data);
}


public function showTagsAction() {

    $news = Tags::find(array("order"=>"id DESC"));
    $taginfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $taginfo;
}

public function relatedTagsAction($categoryid) {

    $newstags = Newscategory::find(array("categoryid=". $categoryid . ""));
    $data = array();
    foreach ($newstags as $m) {

        $news = News::findFirst("newsid='" . $m->newsid. "'");
        if($news)
        {
           $data[] = array(
            'newsid' => $news->newsid,
            'title' => $news->title,
            'newsslugs' => $news->newsslugs,
            'author' => $news->author,
            'body' => $news->body,
            'banner' => $news->banner,
            'category' => $news->category,
            'status' => $news->status,
            'date' => $news->date,
            'metatitle' => $news->metatitle,
            'metadesc' => $news->metadesc,
            'keyword' => $news->metakeyword
            );
       }


   }

   $relatedTags = json_encode($data, JSON_NUMERIC_CHECK);
   echo $relatedTags;
}


public function newrssAction($offset) {

    $news = News::find(array("order"=>"date DESC", "limit" => array("number" => 10, "offset" => $offset)));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}




}
