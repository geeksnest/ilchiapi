<?php

namespace Controllers;

use \Models\Members as Members;
use \Models\Donationlog as Donationlog;
use \Models\Donation as Donation;
use \Models\Memberconfirmation as Memberconfirmation;
use PHPMailer as PHPMailer;

class MembersController extends \Phalcon\Mvc\Controller {

    public function registrationAction() {
        $data = array();
        if ($_POST) {
            $userName = Members::findFirst("username='" . $_POST['username'] . "'");
            $userEmail = Members::findFirst("email='" . $_POST['email'] . "'");
            if ($userName == true || $userEmail == true) {
                ($userName == true) ? $data["usernametaken"] = "Username already taken." : '';
                ($userEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
            } else {
                $user = new Members();
                $password = sha1($_POST['password']);
                $user->assign(array(
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'password' => $password,
                    'birthday' => $_POST['bday']['val'] . '/' . $_POST['bmonth']['val'] . '/' . $_POST['byear']['val'],
                    'gender' => $_POST['gender'],
                    'firstname' => $_POST['fname'],
                    'lastname' => $_POST['lname'],
                    'location' => $_POST['location']['name'],
                    'zipcode' => $_POST['zipcode'],
                    'status' => 0
                ));
                if (!$user->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $user->getMessages()]);
                } else {

                    $confirmation = new Memberconfirmation();
                    $confirmationCode = sha1($user->userid);
                    $confirmation->assign(array(
                        'members_id' => $user->userid,
                        'members_code' => $confirmationCode
                    ));
                    if (!$confirmation->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                        foreach ($confirmation->getMessages() as $message) {
                            echo $message;
                        }
                    } else {
                        $data['success'] = "User profile has been stored.";
                        $mail = new PHPMailer();

                        $mail->isSMTP();
                        $mail->Host = 'smtp.mandrillapp.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = 'efrenbautistajr@gmail.com';
                        $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';                    
                        $mail->Port = 587;

                        $mail->From = 'no-reply@eco.com';
                        $mail->FromName = 'Earth Citizen Organization';
                        $mail->addAddress($_POST['email'], $_POST['username']);

                        $mail->isHTML(true);


                        $mail->Subject = 'Earth Citizen Organization Confirmation Email';
                        $mail->Body = '
							Thank you for registering with the Earth Citizens Organization! 
 <br/><br/>
							Please click the confirmation link below to activate your account. <br/> <br/> Thanks!
							<br/><br/>
							Confirmation Link:
							<br/>
							Code: <a href="http://earthcitizens.org/donation/confirmation/' . $user->userid . '/' . $confirmationCode . '">http://earthcitizens.org/donation/confirmation/' . $user->userid . '/' . $confirmationCode . '	</a>

						';

                        if (!$mail->send()) {
                            $data = array('error' => $mail->ErrorInfo);
                        } else {
                            $data = array('success' => 'success');
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function memberslistAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $members = Members::find();
        } else {
            $conditions = "firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%'";
            $members = Members::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
            "data" => $members,
            "limit" => 10,
            "page" => $currentPage
                )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'username' => $m->username,
                'firstname' => $m->firstname,
                'lastname' => $m->lastname,
                'email' => $m->email,
                'birthday' => $m->birthday,
                'location' => $m->location,
                'zipcode' => $m->zipcode
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'user');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function checkuserAction() {
        if ($_POST) {
            $username = $_POST['email'];
            $password = $_POST['password'];
            //$hashpass = $this->security->hash($password);
            $user = Members::findFirst("email='$username'");
            if ($user) {
                $shapass = sha1($password);
                if ($shapass == $user->password) {
                    if ($user->status == 0) {
                        $data['error'] = 'You have not yet confirmed your email address. Please check your email';
                        echo json_encode($data);
                        return;
                    } else {
                        $data['success'] = $user->firstname;
                        echo json_encode($data);
                        return;
                    }
                }
            }
        }
        $data['error'] = 'Please enter correct email and password.';
        echo json_encode($data);
        return;
    }

    public function paypalipn() {
        
    }

    public function userdonationAction() {
        //Executing a simple query
        // How many different areas are assigned to employees?
        //$don = Donation::findFirst("id=1");
        $usercount = Donationlog::count(array("distinct" => "useremail"));
        $members = Donationlog::sum(array("column" => "amount"));
        //echo json_encode(array("user" => $don->usercount, "donations" => $don->amount));
        echo json_encode(array("user" => $usercount, "donations" => $members));
    }

    public function donationAction() {
        // Create an album
        $don = Donation::findFirst("id=1");
        //Save both records
        $don->assign(array(
            'amount' => $_POST['amount'],
            'usercount' => $_POST['users']
        ));
        if ($don->save()) {
            echo json_encode(array('amount' => $don->amount, 'users' => $don->amount));
        } else {
            echo json_encode(array('error' => "MAY ERROR"));
        }
    }

    public function sendmailAction() {
        $type = $_POST['type'];
        $email = $_POST['email'];
        if ($type == "createPassword") {
            $don = Members::findFirst("email='" . $email . "'");
            $a = '';
            for ($i = 0; $i < 6; $i++) {
                $a .= mt_rand(0, 9);
            }
            $don->password = sha1($a);
            //Save both records
            if ($don->save()) {
                $data['success'] = "A temporary generated password was sent in your email!";
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                //$mail->SMTPSecure = 'ssl';                    
                $mail->Port = 587;

                $mail->From = 'no-reply@eco.com';
                $mail->FromName = 'Earth Citizen Organization';
                $mail->addAddress($email, 'Earth Citizen');

                $mail->isHTML(true);


                $mail->Subject = 'Earth Citizen Organization Password Changed';
                $mail->Body = '
							We have generated a new password for you.
 <br/><br/>
							The site does not have yet a profile feature which you could edit your information so please use your generated password as of the moment. <br/> <br/> Thanks!
							<br/><br/>
							Generated Password:
							<br/>' . $a;

                if (!$mail->send()) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
            } else {
                $data = array('error' => $mail->ErrorInfo);
            }
            echo json_encode($data);
        } else if ($type == "resendActivation") {
            $don = Members::findFirst("email='" . $email . "'");
            if ($don->status == 0) {

                $mem = Memberconfirmation::findFirst("members_id='" . $don->userid . "'");
                $confirmationCode = sha1($don->userid);
                $mem->members_code = $confirmationCode;
                //Save both records
                if ($mem->save()) {
                    $data['success'] = "A temporary generated password was sent in your email!";
                    $mail = new PHPMailer();

                    $mail->isSMTP();
                    $mail->Host = 'smtp.mandrillapp.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'efrenbautistajr@gmail.com';
                    $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                    //$mail->SMTPSecure = 'ssl';                    
                    $mail->Port = 587;

                    $mail->From = 'no-reply@eco.com';
                    $mail->FromName = 'Earth Citizen Organization';
                    $mail->addAddress($email, $don->firstname . ' ' . $don->lastname);

                    $mail->isHTML(true);


                    $mail->Subject = 'Earth Citizen Organization Confirmation Email';
                    $mail->Body = '
							Thank you for registering with the Earth Citizens Organization! 
 <br/><br/>
							Please click the confirmation link below to activate your account. <br/> <br/> Thanks!
							<br/><br/>
							Confirmation Link:
							<br/>
							Code: <a href="http://www.earthcitizens.org/index/confirmation/' . $don->userid . '/' . $confirmationCode . '">http://www.earthcitizens.org/confirmation/' . $don->userid . '/' . $confirmationCode . '	</a>

						';

                    if (!$mail->send()) {
                        $data = array('error' => $mail->ErrorInfo);
                    } else {
                        $data = array('success' => 'success');
                    }
                } else {
                    $data = array('error' => $mail->ErrorInfo);
                }
                echo json_encode($data);
            } else {
                $data = array('error' => 'You are already activated or you have not yet registered.');
                echo json_encode($data);
            }
        } else {
            var_dump($_POST);
        }
    }

    public function membersinfoAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($members) {
            $bd = explode('/', $members->birthday);
            $data = array(
                'username' => $members->username,
                'email' => $members->email,
                'firstname' => $members->firstname,
                'lastname' => $members->lastname,
                'bday' => $bd[0],
                'bmonth' => $bd[1],
                'byear' => $bd[2],
                'gender' => $members->gender,
                'location' => $members->location,
                'zipcode' => $members->zipcode,
                'status' => $members->status,
            );
        }
        echo json_encode($data);
    }

    public function memberupdateAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));

        if ($members) {
            $checkusername = Members::findFirst(array("username = '" . $_POST['username'] . "' AND userid != '" . $members->userid . "'"));
            $checkemail = Members::findFirst(array("email = '" . $_POST['email'] . "' AND userid != '" . $members->userid . "'"));
            if ($checkusername) {
                $data = array('usernametaken' => 'Username already taken.');
            } else if ($checkemail) {
                $data = array('emailtaken' => 'Email address already taken.');
            } else {
                $members->username = $_POST['username'];
                $members->email = $_POST['email'];
                if (!empty($_POST['bday']['val']) && !empty($_POST['bmonth']['val']) && !empty($_POST['byear']['val'])) {
                    $members->birthday = $_POST['bday']['val'] . '/' . $_POST['bmonth']['val'] . '/' . $_POST['byear']['val'];
                }
                $members->gender = $_POST['gender'];
                $members->firstname = $_POST['fname'];
                $members->lastname = $_POST['lname'];
                if (!empty($_POST['location']['name'])) {
                    $members->location = $_POST['location']['name'];
                }
                var_dump($_POST['status']);
                if ($_POST['status'] == 'true') {
                    $members->status = 1;
                } else {
                    $members->status = 0;
                }
                $members->zipcode = $_POST['zipcode'];

                //Save both records
                if ($members->save()) {
                    $data['success'] = "Your profile has been updated";
                    $mail = new PHPMailer();

                    $mail->isSMTP();
                    $mail->Host = 'smtp.mandrillapp.com';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'efrenbautistajr@gmail.com';
                    $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                    //$mail->SMTPSecure = 'ssl';                    
                    $mail->Port = 587;

                    $mail->From = 'no-reply@eco.com';
                    $mail->FromName = 'Earth Citizen Organization';
                    $mail->addAddress($members->email, $members->firstname . ' ' . $members->lastname);

                    $mail->isHTML(true);


                    $mail->Subject = 'Earth Citizen Organization Admin Updated your Profile.';
                    $mail->Body = '
			Your profile has been updated! Check below:
                        <br/><br/>
                            Username: ' . $members->username . ' <br/>
                            Email: ' . $members->email . ' <br/>
                            Birthday: ' . $members->birthday . ' <br/>
                            Gender: ' . $members->gender . ' <br/>
                            Firstname: ' . $members->firstname . ' <br/>
                            Lastname: ' . $members->lastname . ' <br/>
                            Location: ' . $members->location . ' <br/>
                            Zipcode: ' . $members->zipcode . ' <br/>
                            Status: ' . ($members->status == 0 ? 'Inactive' : 'Activated' ) . ' <br/>
						';

                    if (!$mail->send()) {
                        $data = array('error' => $mail->ErrorInfo);
                    } else {
                        $data = array('success' => 'success');
                    }
                } else {
                    $data = array('error' => 'Something went wrong.');
                }
            }
            echo json_encode($data);
        }
    }

    public function memberupdatepasswordAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($members) {
            $password = sha1($_POST['password']);
            $members->password = $password;
            if ($members->save()) {
                $data = array('success' => 'Password Saved');
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                //$mail->SMTPSecure = 'ssl';                    
                $mail->Port = 587;

                $mail->From = 'no-reply@eco.com';
                $mail->FromName = 'Earth Citizen Organization';
                $mail->addAddress($members->email, $members->firstname . ' ' . $members->lastname);

                $mail->isHTML(true);


                $mail->Subject = 'Earth Citizen Organization Admin Changed your Password.';
                $mail->Body = '
			Your profile has been updated! Check below:
                        <br/><br/>
                            New Password: ' . $_POST['password'] . ' <br/>
						';

                if (!$mail->send()) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
            }
        }
        echo json_encode($data);
    }

    public function memberdeleteAction($memberid) {
        $conditions = "userid=" . $memberid;
        $members = Members::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($members) {
            if ($members->delete()) {
                $data = array('success' => 'Member Deleted');
            }
        }
        echo json_encode($data);
    }

}
