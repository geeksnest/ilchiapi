<?php
namespace Controllers;
use \Models\Image as Image;
use \Models\Video_upload  as Vidup;
use \Models\Video as Video;
use \Models\Album  as Album;
class VideoController extends \Phalcon\Mvc\Controller
{
    public function slideruploadAction()
    {
        var_dump($_POST);
        
    }
    /*
    * Move File Upload of Programs 
    */
    public function videouploaderAction($filename){
        $getType=explode('.', $filename);
        $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        $generateid=md5(uniqid(rand(), true));

        if(is_file('../public/server/php/files/'.$filename)){
            rename('../public/server/php/files/'.$filename, '../public/video/'.$filename);
        }
       
        

        $dlttemp = Vidup::find();
        if ($dlttemp) {
            if($dlttemp->delete()){
                // $data = array('success' => 'Photo has Been deleted');
            }
        }
        $imgUpload = new Vidup();
        $imgUpload->assign(array(
            'gen_id'  => $generateid,
            'path' => $filename,
            ));
        if (!$imgUpload->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
            echo json_encode(["error" => $imgUpload->getMessages()]);
        }else(
            $data['success'] = "Video Upload");

        echo json_encode($data);
    }



    public function tempvidAction(){
        $vid_up= Vidup::find();
        foreach ($vid_up as $vids) {
            $data[] = array(
                'vidId' => $vids->gen_id,
                'path'  => $vids->path
            );
        }
        echo json_encode($data);
    }

    public function savevidAction(){
        if( $_POST['option']==0){
            $curpath=$_POST['embed'];
            $width= str_replace('width="560"','', $curpath);
            $path= str_replace('height="315"','', $width);
        }elseif($_POST['option']==1){
           $vid_save= Vidup::find();
           foreach ($vid_save as $vidsup) {
                $path  ='<iframe src="http://ilchiapi/video/'.$vidsup->path.'" frameborder="0" allowfullscreen></iframe>';
            }
        }

        ////YEAR
        if(!isset($_POST['year']))
        {
            $year_s="0000";
        }
        else{
            $year_s=$_POST['year'];
            if($year_s=="" || $year_s ==0){
                $year_s="0000";
            }
        }
        ////MONTH
         if(!isset($_POST['month']))
        {
            $month_s="00";
        }
        else{
            $month_s=$_POST['month'];          
            if($month_s=="" || $month_s ==0){
                $month_s="00";
            }
        }

          ////MONTH
         if(!isset($_POST['day']))
        {
            $day_s="00";
        }
        else{
            $day_s=$_POST['day'];
            if($day_s=="" || $day_s ==0){
            $day_s="00";
            }
        }

       

       

         $date= $year_s."-".$month_s."-".$day_s;
        
        $submitdata = new Video();
        $submitdata->assign(array(
            
            'title'         => $_POST['title'],
            'slugs'         => $_POST['slugs'],
            'album'         => $_POST['categories'],
            'upload_categ'  => $_POST['option'],
            'path'          => $path,
            'description'   => $_POST['description'],
            'location'      => $_POST['location'],
            'duration'      => $_POST['duration'],
            'date'          => $date,
            'language'      => $_POST['language'],
            'type'          => "Videos",
            'featvid'       => 0,
            ));
        if (!$submitdata->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        }
        else{
            $data['success'] = "Save video.";
        }

        echo json_encode($data);
     
    }

    public function videolistdAction($num, $page, $keyword){
       if ($keyword == 'null' || $keyword == 'undefined') {
        
            $album = Video::find(array("order" => "id DESC"));
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'";
            $album= Video::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
            "data" => $album,
            "limit" => 15,
            "page" => $currentPage
                )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id'  => $m->id ,  
                'title'  => $m->title ,       
                'album'  => $m->album ,       
                'upload_categ' => $m->upload_categ , 
                'path'  => $m->path ,          
                'description' => $m->description ,  
                'location'  => $m->location ,  
                'duration' => $m->duration ,     
                'date'  => $m->date ,         
                'language'  => $m->language,
                'featvid'  => $m->featvid
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }



    public function videoindexAction($offset){
        $testimonial = Video::find(array("order"=>"date DESC", "limit" => array("number" => 100, "offset" => $offset)));
        echo  $msginfo= json_encode($testimonial->toArray(), JSON_NUMERIC_CHECK);
        
    }

    public function dltvideoAction($id){
        $dltVideo = Video::findFirst('id='.$id.'');
        $data = array('error' => 'Not Found');
        if ($dltVideo) {
            if($dltVideo->delete()){
                $data = array('success' => 'Testimonial has Been deleted');
            }
        }
        echo json_encode($data);
    }

     public function getdispAction($id){
        $videoview = Video::findFirst("id='" .$id."'");
        $info = array();
        if ($videoview) {
            $year= substr($videoview->date, 0,4);
            $temp_month= substr($videoview->date, 5,2);
            $day= substr($videoview->date, 8,2);

            if(substr($temp_month,0,1)==0){
                $month=substr($temp_month,1,1);
            }
            else{
                $month=$temp_month;
            }
            $info = array(
                'id'            => $videoview->id ,  
                'title'         => $videoview->title ,
                'slugs'         => $videoview->slugs ,        
                'album'         => $videoview->album ,       
                'upload_categ'  => $videoview->upload_categ , 
                'path'          => $videoview->path ,          
                'description'   => $videoview->description ,  
                'location'      => $videoview->location ,  
                'duration'      => $videoview->duration ,     
                'year'          => $year ,
                'month'         => $temp_month,  
                'day'           => $day ,           
                'language'      => $videoview->language  
            );
        }
        echo json_encode($info);
    }

     public function updatevidAction(){
         $id =$_POST['id'];  
        if(isset($_POST['option'])){ 
       
         ///START IF  
          if( $_POST['option']==0){

            $curpath=$_POST['embed'];
            $width= str_replace('width="560"','', $curpath);
            $path= str_replace('height="315"','', $width);

        }else if($_POST['option']==1){
         $vid_save= Vidup::find();
         foreach ($vid_save as $vidsup) {
             $path  ='<iframe src="http://ilchiapi/video/'.$vidsup->path.'" frameborder="0" allowfullscreen></iframe>';
         }
     } 
 }else{
    $addquery_path      = '';
    $addquery_option    = '';
 }      

        $year_s=$_POST['year'];
        $month_s=$_POST['month'];
        $day_s=$_POST['day'];

        if($year_s=="" || $year_s ==0){
           $year_s="0000";
        }
          if($month_s=="" || $month_s ==0){
           $month_s="00";
        }
          if($day_s=="" || $day_s ==0){
           $day_s="00";
        }

         $date= $year_s."-".$month_s."-".$day_s;
         $Video = Video::findFirst('id='.$id.'');
        
         $Video->title          =$_POST['title'];
         $Video->slugs          =$_POST['slugs'];
         $Video->album          = $_POST['album'];
         
         if(isset($_POST['option'])){
            $Video->path = $path;
            $Video->upload_categ   =$_POST['option']; 
         }
        
         $Video->description    =$_POST['description'];
         $Video->location       =$_POST['location'];
         $Video->duration       =$_POST['duration'];
         $Video->date           =$date;
         $Video->language       =$_POST['language'];


         if(!$Video->save()){
           $data['error'] = "Something went wrong saving the data, please try again.";
        }else{
            $data['success'] = "Data Save.";
        }
       echo json_encode($data);
    }



    //  public function videoshowAction($id){
    //     $vid = Video::find(array("id='" .$id."'"));

    // }

     public function videoshowAction($slugs){
        $vid = Video::find(array("slugs='" .$slugs."'"));

        echo  $msginfo= json_encode($vid->toArray(), JSON_NUMERIC_CHECK);
        
    }

     public function showmoreAction(){
        $vid = Video::find(array("order" => "id DESC"));
        echo  $msginfo= json_encode($vid->toArray(), JSON_NUMERIC_CHECK);
        
    }

     public function latestvidAction(){
        $vid = Video::find(array("order"=>"id DESC limit 1"));
        echo  $latestvid= json_encode($vid->toArray(), JSON_NUMERIC_CHECK);
        
    }

    public function featuredvidAction(){
        $vid = Video::find(array("featvid='1'","order"=>"id DESC "));
        echo  $latestvid= json_encode($vid->toArray(), JSON_NUMERIC_CHECK);
        
    }

    public function setasfeatAction($id){

        $Video = Video::findFirst('id='.$id.'');
        $Video->featvid =1;
         if(!$Video->save()){
           $data="error";
           var_dump($Video->getMessages());
        }else{
            $data="Add to Featured Video Success";
        }
        echo json_encode($data);  
    }
    public function removefeatAction($id){
        $Video = Video::findFirst('id='.$id.'');
        $Video->featvid =0;
         if(!$Video->save()){
           $data="error";
        }else{
            $data="Remove From List Success";
        }
        echo json_encode($data);
        
    }



    
       
    
}

