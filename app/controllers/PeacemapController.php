<?php

namespace Controllers;

use \Models\Peacemaps as Peacemaps;
use \Models\Peacemaplocations as Peacemaploc;

class PeacemapController extends \Phalcon\Mvc\Controller {

    public function createAction() {
        if ($_POST) {
            $peacemap = new Peacemaps();
            $peacemap->assign(array(
                'title' => $_POST['title'],
                'description' => $_POST['description'],
                'lat' => $_POST['lat'],
                'long' => $_POST['long']
            ));
            $data = [];
            if (!$peacemap->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                for ($x = 0; $x < count($_POST['coordinates']); $x++) {
                    $peacemaploc = new Peacemaploc();
                    $loc = $_POST['coordinates'][$x];
                    $peacemaploc->assign(array(
                        'longaaa' => $loc['long'],
                        'labelContent' => $loc['labelContent'],
                        'lataaa' => $loc['lat'],
                        'peacemapid' => $peacemap->id
                    ));

                    if (!$peacemaploc->save()) {
                        var_dump($peacemaploc->getMessages());
                        $data['error'] = 'Cannot Save your Map!';
                    } else {
                        $data['success'] = $_POST['coordinates'];
                    }
                    var_dump($_POST['coordinates'][$x]);
                }
            }
            echo json_encode($data);
        }
    }

    public function updateAction() {

        if ($_POST) {
            $conditions = "id=" . $_POST['id'];
            $peacemap = Peacemaps::findFirst(array($conditions));

            $data = [];
            $peacemap->title = $_POST['title'];
            $peacemap->description = $_POST['description'];

            $data = [];
            if (!$peacemap->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {

                $peacmaploc = Peacemaploc::find(array('peacemapid=' . $_POST['id']));
                if ($peacmaploc != false) {
                    if ($peacmaploc->delete() == false) {
                        echo "Sorry, we can't delete the robot right now: \n";
                        foreach ($peacmaploc->getMessages() as $message) {
                            echo $message, "\n";
                        }
                    } else {
                        echo "The robot was deleted successfully!";
                    }
                }

                for ($x = 0; $x < count($_POST['coordinates']); $x++) {
                    $peacemaploc = new Peacemaploc();
                    $loc = $_POST['coordinates'][$x];
                    $peacemaploc->assign(array(
                        'longaaa' => $loc['long'],
                        'labelContent' => $loc['labelContent'],
                        'lataaa' => $loc['lat'],
                        'peacemapid' => $peacemap->id
                    ));

                    if (!$peacemaploc->save()) {
                        var_dump($peacemaploc->getMessages());
                        $data['error'] = 'Cannot Save your Map!';
                    } else {
                        $data['success'] = $_POST['coordinates'];
                    }
                    var_dump($_POST['coordinates'][$x]);
                }
            }
            echo json_encode($data);
        }
    }

    public function listAction() {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $peacemarks = Peacemaps::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' OR description LIKE '%" . $keyword . "%'";
            $peacemarks = Peacemaps::find(array($conditions));
        }

        $currentPage = (int) ($peacemarks);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
            "data" => $peacemarks,
            "limit" => 10,
            "page" => $currentPage
                )
        );

        // Get the paginated results
        $peace = $paginator->getPaginate();

        $data = array();
        foreach ($peace->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'description' => $m->description
            );
        }
        $p = array();
        for ($x = 1; $x <= $peace->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $peace->current, 'before' => $peace->before, 'next' => $peace->next, 'last' => $peace->last, 'total_items' => $peace->total_items));
    }

    public function deleteAction($id) {
        $conditions = "id=" . $id;
        $peace = Peacemaps::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($peace->delete()) {
            $conditionsloc = "peacemapid=" . $id;
            $peaceloc = Peacemaploc::find(array($conditionsloc));
            if ($peaceloc->delete()) {
                $data = array('success' => 'Peacemap Deleted');
            }
            $data = array('success' => 'Peacemap Deleted');
        }
        echo json_encode($peace);
    }

    public function getmapAction($id) {
        $conditions = "id=" . $id;
        $peace = Peacemaps::find(array($conditions));
        $peaceArray = $peace->toArray();
        $conditionsloc = "peacemapid=" . $id;
        $peaceloc = Peacemaploc::find(array($conditionsloc))->toArray();
        $peaceArray[0]['peacelocations'] = $peaceloc;
        //var_dump($peaceArray);
        echo json_encode($peaceArray, JSON_NUMERIC_CHECK);
    }

}
