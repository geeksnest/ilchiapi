<?php
namespace Controllers;
use \Models\Publicationimage as Pubimage;
use \Models\Publication as Publication;
class PublicationController extends \Phalcon\Mvc\Controller
{
    public function slideruploadAction()
    {
        var_dump($_POST);
        
    }
    /*
    * Move File Upload of Programs 
    */
    public function ajaxfileuploaderAction($filename, $type){
        $newpicname = 0;

        $getType=explode('.', $filename);
        $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        $generateid=md5(uniqid(rand(), true));

        if(is_file('../public/server/php/files/'.$filename)){
            rename('../public/server/php/files/'.$filename, '../public/images/pubbanner/'.$newfileName);
        }
        $dlttemp = Pubimage::find();
        if ($dlttemp) {
            if($dlttemp->delete()){
                // $data = array('success' => 'Photo has Been deleted');
            }
        }
         $imgUpload = new Pubimage();
         $imgUpload->assign(array(
                    'generateid'  => $generateid,
                    'description' => 'Description Here',
                    'path' => $newfileName,
                    'title' => 'Title Here'
                ));
          if (!$imgUpload->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $imgUpload->getMessages()]);
                }else{
        $getImage= Pubimage::find(array("order" => "id DESC"));
        foreach ($getImage as $getImages) {
            $data[] = array(
                'imgid' => $getImages->id,
                'description' => $getImages->description,
                'imgpath' => $getImages->path,
                'imgtitle' => $getImages->title
            );
        }
        echo json_encode($data);
        } 
       
    }

    /*
    * Display Content
    */
    public function imagelistAction(){

        $getImage= Pubimage::find();
        $count=count($getImage);
        if($count==0){
            $images[]=array(
                'imgpath'  => "default.png",
                );
        }else{
           foreach ($getImage as $getImages) {
            $images[] = array(
                'imgid'   => $getImages->id,
                'description'   => $getImages->description,
                'imgpath'          => $getImages->path,
                'imgtitle'          => $getImages->title
                );
            }
        }
       
        echo json_encode($images);
    }
    //  public function updateinfoimgAction(){
    //     $id = $_POST['id'];
    //     $sliderimages = Pubimage::findFirst('id='.$id.' ');
    //     $sliderimages->description = $_POST['description'];
    //     $sliderimages->title= $_POST['title'];
    //     if(!$sliderimages->save()){
    //         echo 'Error';
    //     }else{

    //     }
    // }

    public function dltphotoAction($id){
        $dltPhoto = Pubimage::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

    public function createPageAction() {
        $path='';
        $data = array();
        $getImage= Pubimage::find();
        foreach ($getImage as $getImages) {
            $path=$getImages->path;

        }
        if($path!=""){

           if ($_POST) {
            $status = 0;

            if ($_POST['check']) {
                $status = 1;
            } else {
                $status = 0;
            }
            $page = new Publication();
            $page->assign(array(
                'title' => $_POST['title'],
                'content' => $_POST['content'],
                'price' => $_POST['price'],
                'reference' => $_POST['reflink'],
                'category' => $_POST['category'],
                'banner' => $path,
                'active' => $status,
                'metatitle' => $_POST['metatitle'],
                'metadesc' => $_POST['metadesc'],
                'metakeyword' => $_POST['keyword']
                ));
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
            $dlttemp = Pubimage::find();
            if ($dlttemp) {
                if($dlttemp->delete()){
                    // $data = array('success' => 'Photo has Been deleted');
                }
            }
        }
    }
    else{
        $data=0;
    }
    echo json_encode($data);
}

     public function publistAction() {
        $publication = Publication::find(array("active='1'","order"=>"title ASC"));
        echo  $info= json_encode($publication->toArray(), JSON_NUMERIC_CHECK);
       
    }




    public function managepubAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $album = Publication::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%'";
            $album= Publication::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
            "data" => $album,
            "limit" => 15,
            "page" => $currentPage
                )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'content' => $m->content,
                'price' => $m->price,
                'reference' => $m->reference,
                'category' => $m->category,
                'banner' => $m->banner,
                'active' => $m->active,
                'metatitle' => $m->metatitle,
                'metadesc' => $m->metadesc,
                'keyword' => $m->metakeyword
            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function deletepubAction($id){
    
        $dltPub = Publication::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPub) {
            if($dltPub->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }else{
                $data = array('error' => 'Not Found');
            }
        }
        echo json_encode($data);
    }


    public function dispeditAction($id){
        // $data=$id;
        $getpub= Publication::find('id='.$id.'');
        foreach ($getpub as $m) {
            $data[] = array(
                'id' => $m->id,
                'title' => $m->title,
                'content' => $m->content,
                'price' => $m->price,
                'reference' => $m->reference,
                'category' => $m->category,
                'banner' => $m->banner,
                'active' => $m->active,
                'metatitle' => $m->metatitle,
                'metadesc' => $m->metadesc,
                'keyword' => $m->metakeyword,
            );
        }
    
       echo json_encode($data);
    }


     public function updatepubAction() {
        $id=$_POST['id'];    
        $data = array();
        $getImage= Pubimage::find();
        foreach ($getImage as $getImages) {
            $path=$getImages->path;
           
        }
         if ($_POST) {
            $status = 0;

            if ($_POST['active']) {
                $status = 1;
            } else {
                $status = 0;
            }
             
             $data=$id;   
             $pub = Publication::findFirst('id='.$id.'');
             $pub->title       =$_POST['title'];
             $pub->content     = $_POST['content'];
             $pub->price       =$_POST['price'];
             $pub->reference   =$_POST['reference'];
             $pub->category    =$_POST['category'];
             $pub->banner      =$_POST['banner'];
             $pub->active      =$status;
             $pub->metatitle   =$_POST['metatitle'];
             $pub->metadesc    =$_POST['metadesc'];
             $pub->metakeyword =$_POST['keyword']; 

         if(!$pub->save()){
           $data="error";
        }else{
            $data="Success";
        }
        }
        echo json_encode($data);
    }





}

