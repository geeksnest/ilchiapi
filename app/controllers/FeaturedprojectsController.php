<?php
namespace Controllers;

use \Models\Featuredprojects as Featuredprojects;
use \Models\Featuredphoto as Featuredphoto;
use \Models\Album as Album;
use \Models\Image as Image;
use \Models\Subscribers as Subscribers;

class FeaturedprojectsController extends \Phalcon\Mvc\Controller
{

    public function createfeaturedprojectAction(){

    }

    // List slides

    public function slidesAction(){

        $getSlide = Album::find(array("order" => "id DESC"));
        foreach ($getSlide as $getSlides) {;
            $data[] = array(
                'album_name'=>$getSlides->album_name
                );
        }
        echo json_encode($data);
    }    

    // Manage Features

    public function managefeaturedprojectAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Featuredprojects::find();
        } else {
            $conditions = "feat_title LIKE '%" . $keyword . "%'";
            $Pages = Featuredprojects::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'feat_id' => $m->feat_id,
                'feat_title' => $m->feat_title,
                'feat_content' => $m->feat_content,
                'feat_status' => $m->feat_status,
                'feat_pub' => $m->feat_pub,
                'feat_picpath' => $m->feat_picpath,
                'pubtxt' => $m->pubtxt
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array(
            'data' => $data, 
            'pages' => $p, 
            'index' => $page->current, 
            'before' => $page->before, 
            'next' => $page->next, 
            'last' => $page->last, 
            'total_items' => $page->total_items
            ));

    }

    // Insert feature data to dbase

    public function featdataAction(){
        $data = array();
        if ($_POST){

            $status = 0;
            $pub = 0;

            if($_POST['check']){

                $status = 1;

                $pageSlideupd = Featuredprojects::findFirst('feat_status=1');
                if($pageSlideupd){
                    $pageSlideupd->feat_status = 0;
                    if (!$pageSlideupd->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    }else {
                        $data['success'] = "Success";
                    }
                }

            }else {
                $status = 0;
            }

            if($_POST['check2']){
                $pub = 1;
                $pubtxt = 'Publish';
            }else {
                $pub = 0;
                $pubtxt = 'Unpublish';
            }

            $page = new Featuredprojects();

            $page->assign(array(
                'feat_title' => $_POST['title'],
                'feat_content' => $_POST['body'],
                'feat_picpath' => $_POST['banner'],
                'feat_status' => $status,
                'feat_pub' => $pub,
                'pubtxt' => $pubtxt,
                'feat_loc' => 2,
                'feat_date' => date('Y-m-d'),
                'type' => 'Projects',
                'metatitle' => $_POST['metatitle'],
                'metadesc' => $_POST['metadesc'],
                'metakeyword' => $_POST['keyword']
                ));

            if (!$page->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }else {
                $data['success'] = "Success";
            }
            echo json_encode($data); 
        }
    }    

    // Update feature

    public function featureUpdateAction(){
        $data = array();
        if ($_POST){

            $status = 0;

            if($_POST['check'] == true){
                $status = 1;
                $pageSlideupd = Featuredprojects::findFirst('feat_status=1');
                if($pageSlideupd){
                    $pageSlideupd->feat_status = 0;
                    if (!$pageSlideupd->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    }else{
                        $data['success'] = "Success";
                    }
                }
            }else{
               $status = 0;
           }

           if($_POST['check2']){
            $pub = 1;
            $pubtxt = 'Publish';
        }else{
            $pub = 0;
            $pubtxt = 'Unpublish';
        }

        $pageid = $_POST['pageid'];
        $page = Featuredprojects::findFirst('feat_id='.$pageid.' ');
        $page->feat_title = $_POST['title'];
        $page->feat_content = $_POST['body'];
        $page->feat_picpath = $_POST['banner'];
        $page->feat_status = $status;
        $page->feat_pub = $pub;
        $page->pubtxt = $pubtxt;
        $page->feat_date = date('Y-m-d');
        $page->metatitle   =$_POST['metatitle'];
        $page->metadesc    =$_POST['metadesc'];
        $page->metakeyword =$_POST['keyword']; 

        if (!$page->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";

        } else {
            $data['success'] = "Success";
        }
    }
    echo json_encode($data);
}

    // Upload Image

public function slideruploadAction(){
    var_dump($_POST);
}


public function ajaxfileuploaderAction($filename){
    $newpicname = 0;        
    $getType=explode('.', $filename);
                // New Image Name
    $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);
    $generateid=md5(uniqid(rand(), true));

    if(is_file('../public/server/php/files/'.$filename)){
        rename('../public/server/php/files/'.$filename, '../public/images/featurebanner/'.$newfileName);
    }

    if(is_file('../public/server/php/files/thumbnail/'.$filename)){
        rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/featurebanner/thumbnail/'.$newfileName);
    }

    $imgUpload = new Featuredphoto();
    $imgUpload->assign(array('path' => $newfileName, 'title' => 'Title Here'));
    if (!$imgUpload->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
        echo json_encode(["error" => $imgUpload->getMessages()]);
    }else{
        $getImage= Featuredphoto::find(array("order"=>"id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array('id'=>$getImages->id, 'imgpath'=>$getImages->path, 'imgtitle'=>$getImages->title);
        }
        echo json_encode($data);
    }       
}

            // List Uploaded Images

public function imagelistAction(){

    $getImage= Featuredphoto::find(array("order" => "id DESC"));
    foreach ($getImage as $getImages) {;
        $data[] = array(
            'id'=>$getImages->id,
            'imgpath'=>$getImages->path,
            'imgtitle'=>$getImages->title
            );
    }
    echo json_encode($data);

}

            // Delete Uploaded Images

public function dltphotoAction(){
    $id = $_POST['id'];
    $dltPhoto = Featuredphoto::findFirst('id='.$id.' ');
    $data = array('error' => 'Not Found');
    if ($dltPhoto) {
        if($dltPhoto->delete()){
            $data = array('success' => 'Photo has Been deleted');
        }
    }
    echo json_encode($data);
}

            // Delete Uploaded Images

public function dltfeatphotoAction(){
    $id = $_POST['id'];
    $dltPhoto = Featuredphoto::findFirst('id='.$id.' ');
    $data = array('error' => 'Not Found');
    if ($dltPhoto) {
        if($dltPhoto->delete()){
            $data = array('success' => 'Photo has Been deleted');
        }
    }
    echo json_encode($data);
}

            // view edit layout

public function featureinfoAction($pageid) {

    $pages = Featuredprojects::findFirst("feat_id=" . $pageid);
    $data = array();
    if ($pages) {
        $data = array(
            'pageid' => $pages->feat_id,
            'title' => $pages->feat_title,
            'body' => $pages->feat_content,
            'banner' => $pages->feat_picpath,
            'check' => $pages->feat_status,
            'check2' => $pages->feat_pub,
            'feat_date' => $pages->feat_date,
            'metatitle' => $pages->metatitle,
            'metadesc' => $pages->metadesc,
            'keyword' => $pages->metakeyword
            );
    }
    echo json_encode($data);
}

public function featuredeleteAction($pageid) {
    $conditions = "feat_id=" . $pageid;
    $page = Featuredprojects::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($page) {
        if($page->delete()){
            $data = array('success' => 'Featured Project Deleted');
        }
    }
    echo json_encode($data);
}

public function editfeaturedprojectAction()
{


}

    // view layout

public function featureviewAction() {

    $pages = Featuredprojects::findFirst('feat_status=1');
    $data = array();
    if ($pages) {
        $album = Album::findFirst('album_name="'.$pages->slidepath.'"');
        $image = Image::find('folderid="'.$album->album_id.'"');
        $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
        $data = array(
            'feat_id' => $pages->feat_id,
            'title' => $pages->feat_title,
            'body' => $pages->feat_content,
            'banner' => $pages->feat_picpath,
            'check' => $pages->feat_status,
            'slides' => $slides,
            'check2' => $pages->feat_pub,
            'feat_date' => $pages->feat_date,
            'metatitle' => $pages->metatitle,
            'metadesc' => $pages->metadesc,
            'keyword' => $pages->metakeyword
            );
    }
    echo json_encode($data);
}

    // view layout


public function listactAction($pageid) {

    $pages = Featuredprojects::findFirst("feat_id=" . $pageid);
    $data = array();
    if ($pages) {
            //$album = Album::findFirst('album_name="'.$pages->slidepath.'"');
        $image = Image::find('foldername="'.$pages->slidepath.'"');
        $pslides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
        $data = array(
            'pageid' => $pages->feat_id,
            'title' => $pages->feat_title,
            'body' => $pages->feat_content,
            'banner' => $pages->feat_picpath,
            'check' => $pages->feat_status,
            'pslides' => $pslides,
            'check2' => $pages->feat_pub,
            'feat_date' => $pages->feat_date,
            'metatitle' => $pages->metatitle,
            'metadesc' => $pages->metadesc,
            'keyword' => $pages->metakeyword
            );
    }
    echo json_encode($data);
}

    // show more

public function featuredsAction($offset) {
    $projik = Featuredprojects::find(array("order"=>"feat_status DESC", "limit" => array("number" => 5, "offset" => $offset)));
    $projikinfo = json_encode($projik->toArray(), JSON_NUMERIC_CHECK);
    echo $projikinfo;
}


public function showProjectAction($offset) {

    $project = Featuredprojects::find(array("order"=>"feat_status DESC", "limit" => array("number" => 4, "offset" => $offset)));
    $projectinfo = json_encode($project->toArray(), JSON_NUMERIC_CHECK);
    echo $projectinfo;
}

public function featuredprojectsAction() {

    $project = Featuredprojects::findFirst("feat_status=1");
    $data = array();
    if ($project) {
        $data = array(
            'feat_id' => $project->feat_id,
            'feat_title' => $project->feat_title,
            'feat_content' => $project->feat_content,
            'feat_picpath' => $project->feat_picpath,
            'feat_date' => $project->feat_date,
            'metatitle' => $project->metatitle,
            'metadesc' => $project->metadesc,
            'keyword' => $project->metakeyword
            );
    }
    echo json_encode($data);
}

public function fullprojectsAction($feat_id) {

    $project = Featuredprojects::findFirst("feat_id='".$feat_id."'");
    $data = array();
    if ($project) {
        $data = array(
            'feat_id' => $project->feat_id,
            'feat_title' => $project->feat_title,
            'feat_content' => $project->feat_content,
            'feat_picpath' => $project->feat_picpath,
            'feat_date' => $project->feat_date,
            'metatitle' => $project->metatitle,
            'metadesc' => $project->metadesc,
            'keyword' => $project->metakeyword
            );
    }
    echo json_encode($data);
}


public function indexmanagefeaturedprojectAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $conditions = "feat_status = 0 order by feat_date DESC";
        $Pages = Featuredprojects::find(array($conditions));
    } else {
        $conditions = "feat_title LIKE '%" . $keyword . "%'";
        $Pages = Featuredprojects::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 4,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'feat_id' => $m->feat_id,
            'feat_title' => $m->feat_title,
            'feat_content' => $m->feat_content,
            'feat_status' => $m->feat_status,
            'feat_pub' => $m->feat_pub,
            'feat_picpath' => $m->feat_picpath,
            'pubtxt' => $m->pubtxt
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array(
        'data' => $data, 
        'pages' => $p, 
        'index' => $page->current, 
        'before' => $page->before, 
        'next' => $page->next, 
        'last' => $page->last, 
        'total_items' => $page->total_items
        ));

}





public function featprojAction() {

    $project = Featuredprojects::findFirst("feat_status=1");
    $data = array();
    if ($project) {
        $data = array(
            'id' => $project->feat_id,
            'title' => $project->feat_title,
            'picpath' => $project->feat_picpath,
            'date' => $project->feat_date,
            'metatitle' => $project->metatitle,
            'metadesc' => $project->metadesc,
            'keyword' => $project->metakeyword
            );
    }
    echo json_encode($data);
}




}