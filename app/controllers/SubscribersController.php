<?php
namespace Controllers;

use \Models\Featuredprojects as Featuredprojects;
use \Models\Featuredphoto as Featuredphoto;
use \Models\Subscribers as Subscribers;
use \Models\Members as Members;
use \Models\Album as Album;
use \Models\Image as Image;

class SubscribersController extends \Phalcon\Mvc\Controller
{

    // add subscriber

    public function nmsaddAction(){
        $data = array();
        if($_POST['name'] == ""){
            $name = 'undefined';
        }else{
            $name = $_POST['name'];
        }
        $NMSemail = trim($_POST['email']);

        $SubsEmail = Subscribers::findFirst("NMSemail='" . $NMSemail . "'");
        if ($SubsEmail == true) {
            ($SubsEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
        } else {
                                  
                ($SubsEmail == false) ? $data["emailregister"] = "Thank you for subscribing to newsletter." : '';
                
                $submitdata = new Subscribers();
                $submitdata->assign(array(
                    'name'  => $name,
                    'NMSemail'  => $NMSemail,
                    'NMSstat' => 1,
                    'NMSdate' => date('Y-m-d'),
                    'NMSstatTXT' => 'subscriber'
                ));
                if (!$submitdata->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $submitdata->getMessages()]);
                }else{
                    $data = "Successfuly Save";
                }
            
        }
        echo json_encode($data);
    }

    // subscribers list

    public function subscriberslistAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Subscribers::find(array("order" => "NMSstatTXT DESC"));
        } else {
            $conditions = "NMSemail LIKE '%" . $keyword . "%'";
            $Pages = Subscribers::find(array($conditions));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'NMSid' => $m->NMSid,
                'name' => $m->name,
                'NMSemail' => $m->NMSemail,
                'NMSstat' => $m->NMSstat,
                'NMSdate' => $m->NMSdate,
                'NMSstatTXT' => $m->NMSstatTXT
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array(
            'data' => $data, 
            'pages' => $p, 
            'index' => $page->current, 
            'before' => $page->before, 
            'next' => $page->next, 
            'last' => $page->last, 
            'total_items' => $page->total_items
            ));
    }

    public function subscribersdeleteAction($pageid) {
        $conditions = "NMSid=" . $pageid;
        $page = Subscribers::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Subscriber Deleted');
            }
        }
        echo json_encode($data);
    }

    public function addsubscriberAction(){
        $data = array();

        $NMSemail = trim($_POST['NMSemail']);

        $SubsEmail = Subscribers::findFirst("NMSemail='" . $NMSemail . "'");
        if ($SubsEmail) {
            $data = array('emailtaken' => 'Email address already taken.');
        } else {
            
                $submitdata = new Subscribers();
                $submitdata->assign(array(
                    'name'  => $_POST['NMSname'],
                    'NMSemail'  => $NMSemail,
                    'NMSstat' => 1,
                    'NMSdate' => date('Y-m-d'),
                    'NMSstatTXT' => 'subscriber'
                ));
                if (!$submitdata->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $submitdata->getMessages()]);
                }else{
                   $data['success'] = "Added";
                }
            
        }
        echo json_encode($data);
    }

    public function subscriberinfoAction($pageid) {

        $subscriber = Subscribers::findFirst("NMSid=" . $pageid);
        $data = array();
        if ($subscriber) {
            $data = array(
                'pageid' => $subscriber->NMSid,
                'name' => $subscriber->name,
                'NMSemail' => $subscriber->NMSemail,
                'NMSstat' => $subscriber->NMSstat
                );
        }
        echo json_encode($data);
    }

    public function updatesubscriberAction(){
        $data = array();
        if ($_POST){
            
            if($_POST['NMSstat']){
                $NMSstat = 1;
                echo $NMSstatTXT = 'subscriber';
            }else{
                $NMSstat = 0;
                $NMSstatTXT = 'non-subscriber';
            }

            $pageid = $_POST['pageid'];
            $NMSemail = $_POST['NMSemail'];

            $SubsEmail = Subscribers::findFirst("NMSemail='" . $NMSemail . "' AND NMSid != " . $pageid . "");

            if ($SubsEmail) {
                $data = array('emailtaken' => 'Email address already taken.');
                $data = 1;
            } else {

                    $subscriber = Subscribers::findFirst('NMSid='.$pageid.' ');
                    $subscriber->name = $_POST['name'];
                    $subscriber->NMSemail = $NMSemail;
                    $subscriber->NMSstat = $NMSstat;
                    $subscriber->NMSstatTXT = $NMSstatTXT;

                    if (!$subscriber->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                        echo json_encode(["error" => $submitdata->getMessages()]);
                    } else {
                        $data['success'] = "Success";
                    }
                
            }
        }
        echo json_encode($data);
    }
}