<?php
namespace Controllers;
use \Models\Proposalfiles as Proposalfiles;
use \Models\Proposals as Proposals;
use \Models\Messages as Messages;
use \Models\Repliedmessage as Repliedmessage;
use PHPMailer as PHPMailer;
class ProposalsController extends \Phalcon\Mvc\Controller
{
  public function manageproposalsAction($num, $page, $keyword) {

     if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Messages::find(array("order"=>"status ASC"));
    } else {
        $conditions = "name LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%' OR date LIKE '%" . $keyword . "%'";
        $Pages = Messages::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'id' => $m->id,
            'name' => $m->name,
            'email' => $m->email,
            'message' => $m->message,
            'date' =>$m->date,
            'status' =>$m->status

            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
}





    //SUBMIT PROPOSAL
public function submitproposalAction() {
    $Proposals = new Proposals();
    $Proposals->assign(array(
        'name'  => $_POST['name'],
        'email' => $_POST['email'],
        'company' => $_POST['company'],
        'proposals_body' => $_POST['proposalbody'],
        'file' => $_POST['file'],
        'unread' => 0,
        'date' => date('m-d-Y h:i:s')
        ));
    // move_uploaded_file(date('Y-m-d_h-i-s') . $_POST['file'], '../public/images/prodposalsfile/');
    if (!$Proposals->save()) {
        $Proposals['error'] = "Something went wrong saving the data, please try again.";
        echo json_encode(["error" => $Proposals->getMessages()]);
        return;
    }   
    echo json_encode($Proposals);
}



    //reply PROPOSAL
public function replyproposalAction() {
    var_dump($_POST);
    $data = array();

    $id = $_POST['proposalid'];
    $page = Messages::findFirst('id=' . $id . ' ');
    $page->status = 2;
    if (!$page->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else {
        $data['success'] = "Success";
        $mail = new PHPMailer();

        $mail->isSMTP();
        $mail->Host = 'smtp.mandrillapp.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'efrenbautistajr@gmail.com';
        $mail->Password = '6Xy18AJ15My59aQNTHE5kA';
                        //$mail->SMTPSecure = 'ssl';                    
        $mail->Port = 587;

        $mail->From = 'no-reply@ilchi.com';
        $mail->FromName = 'Ilchi Lee Reply';

        

        $mail->addAddress($_POST['email'], $_POST['name']);
        $mail->AddCC($_POST['cc'], $_POST['name']);
        $mail->isHTML(true);


        $mail->Subject = 'Ilchi Lee Reply';
        $mail->Body = '<div style="background-color: #eee;padding:20px;margin-bottom:10px;">This is the reply to your message: "'.$_POST['message'].'"</div>' . '<br><br> Admin Reply: ' .$_POST['messages'];

        if (!$mail->send()) {
            $data['mail_error'] = $mail->ErrorInfo;
        } else {
            $data['mail_success'] = "Success";



            $repliedmessage = new Repliedmessage();
            $repliedmessage->assign(array(
                'id'  => $id,
                'email' => $_POST['email'],
                'message' => $_POST['messages'],
                'date' => date('Y-m-d')
                ));
            if (!$repliedmessage->save()) {
                $repliedmessage['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $repliedmessage->getMessages()]);
                return;
            }   
            echo json_encode($repliedmessage);



        }
        
    }

    
    echo json_encode($data);                  
    
}


public function viewproposalAction($id) {

    $data = array();
    $viewproposal = Messages::findFirst("id=" . $id);
    
    if ($viewproposal) {
        $viewproposal = array(                
            'id' => $viewproposal->id,
            'name' =>$viewproposal->name,
            'email' =>$viewproposal->email,
            'message' =>$viewproposal->message,
            'date' =>$viewproposal->date,
            'status' => $viewproposal->status,
            );
    }
    echo json_encode($viewproposal);

    $read = Messages::findFirst('id=' . $id . ' ');
    if($read->status == 0){
        $read->status = 1;
        if (!$read->save()) {
            $dataread['error'] = "Something went wrong saving the data, please try again.";
        }
    }
    
}


public function listreplyAction($id)
{
     $data = array();
    $getreply= Repliedmessage::find('id=' . $id . ' ');
    foreach ($getreply as $getreply) {
        $data[] = array(
            'id'=>$getreply->id,
            'email'=>$getreply->email,
            'message'=>$getreply->message,
            'date'=>$getreply->date
            );
    }
    echo json_encode($data);

}


}

