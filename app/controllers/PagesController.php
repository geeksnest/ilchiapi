<?php

namespace Controllers;

use \Models\Donationlog as Donationlog;
use \Models\Donation as Donation;
use \Models\Memberconfirmation as Memberconfirmation;
use PHPMailer as PHPMailer;
use \Models\Pages as Pages;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPageAction() {
        $data = array();
        if($_POST['pagefeatures'] == '')
            {
                $pagefeatures = 0;
            }
            else
            {
                $pagefeatures = $_POST['pagefeatures'];
            }
        if ($_POST) {
            $status = 0;

            if ($_POST['check']) {
                $status = 1;
            } else {
                $status = 0;
            }

            $page = new Pages();
            $page->assign(array(
                'title' => $_POST['title'],
                'pageslugs' => $_POST['slugs'],
                'body' => $_POST['body'],
                'banner' => $_POST['banners'],
                'status' => $status,
                'pagefeatures' => $pagefeatures,
                'type' => 'Pages',
                'metatitle' => $_POST['metatitle'],
                'metadesc' => $_POST['metadesc'],
                'metakeyword' => $_POST['keyword']
                ));

            if (!$page->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function managepagesAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Pages::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
            $Pages = Pages::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'pageid' => $m->pageid,
                'title' => $m->title,
                'pageslugs' => $m->pageslugs,
                'status' => $m->status
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }

    public function pageinfoAction($pageid) {

        $pages = Pages::findFirst("pageid=" . $pageid);
        $data = array();
        if ($pages) {
            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'slugs' => htmlentities($pages->pageslugs),
                'body' => $pages->body,
                'banner' => $pages->banner,
                'check' => $pages->status,
                'pagefeatures' => $pages->pagefeatures,
                'metatitle' => $pages->metatitle,
                'metadesc' => $pages->metadesc,
                'keyword' => $pages->metakeyword
                );
        }
        echo json_encode($data);
    }

    public function pageUpdateAction() {

        $data = array();
        if ($_POST) {
            $status = 0;

            if ($_POST['check'] == true) {
                $status = 1;
            } else {
                $status = 0;
            }

            $pageid = $_POST['pageid'];
            $page = Pages::findFirst('pageid=' . $pageid . ' ');
            $page->title = $_POST['title'];
            $page->pageslugs = $_POST['slugs'];
            $page->body = $_POST['body'];
            $page->banner = $_POST['banner'];
            $page->status = $status;
            $page->pagefeatures = $_POST['pagefeatures'];
            $page->metatitle   =$_POST['metatitle'];
            $page->metadesc    =$_POST['metadesc'];
            $page->metakeyword =$_POST['keyword']; 

            if (!$page->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = "pageid=" . $pageid;
        $page = Pages::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if ($page->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function getPageAction($pageslugs) {
        $conditions = "pageslugs LIKE'" . $pageslugs . "'" ;
        echo json_encode(Pages::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);


    }


    public function getPageprojectAction() {
        echo json_encode(Pages::findFirst(array())->toArray(), JSON_NUMERIC_CHECK);
    }


    public function page404Action($slug) {

     

        $pages = Pages::find("pageslugs = '". $slug ."'");
        
        if(count($pages) == 0 ? $data = true :$data = false );

         echo json_encode($data );     
    }


}
