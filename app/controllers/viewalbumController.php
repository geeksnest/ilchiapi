<?php
namespace Controllers;
use \Models\Image as Image;
use \Models\Album  as Album;
class viewalbumController extends \Phalcon\Mvc\Controller
{
    public function slideruploadAction()
    {
        var_dump($_POST);
        
    }
    /*
    * Move File Upload of Programs 
    */
     public function ajaxfileuploaderAction($filename, $folderName,$folderid){
        // $newpicname = 0;
        $data=$filename."/".$folderName."/".$folderid;
         echo json_encode($data);
        // $getType=explode('.', $filename);
        // $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        // $generateid=md5(uniqid(rand(), true));

        // if(is_file('../public/server/php/files/'.$filename)){
        //     rename('../public/server/php/files/'.$filename, '../public/images/'.$folderName.'/'.$newfileName);
        // }
        // if(is_file('../public/server/php/files/thumbnail/'.$filename)){
        //     rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/'.$folderName.'/thumbnail/'.$newfileName);
        // }
       
       
    }
   

  
    public function managealbumAction($num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined') {
            $album = Album::find();
        } else {
            $conditions = "album_name LIKE '%" . $keyword . "%'";
            $album= Album::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
            "data" => $album,
            "limit" => 15,
            "page" => $currentPage
                )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'album_name' => $m->album_name ,
                'album_id' => $m->album_id,
                'date' => $m->date

            );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function albuminfoAction($albumid) {

        $album = Album::findFirst("album_id='" . $albumid."'");
        $album_info = array();
        if ($album) {
            $album_info = array(
                'album_name' => $album ->album_name,
                'album_id' => $album ->album_id,
                'date' => $album ->date
            );
        }
        echo json_encode($album_info);
    }
    

     public function deletealbumAction($albumname) {
        //************************************************************************************************************************************
        //***** DELETE DIRECTORIES INCLUDING THE CONTENT
        function removefolder($album){
            if(is_dir($album)=== true){
                $folderContents = scandir($album);
                unset($folderContents[0],$folderContents[1]);
                foreach ($folderContents as $content => $contentName) {
                    $currentPath=$album.'/'.$contentName;
                    $filetype=filetype($currentPath);
                    if($filetype=='dir'){
                        removefolder($currentPath);
                    }
                    else{
                        unlink($currentPath);
                    }
                    unset($folderContents[$content]);
                }
            }
            rmdir($album);
        }
        removefolder("images\\".$albumname);

        //************************************************************************************************************************************
        //***** DELETE ALBUM
        
        $dltAlbum = Album::find("album_name='".$albumname."'");
        $data = array('error' => 'Not Found');
        if ($dltAlbum) {
            if($dltAlbum->delete()){
              $data = array('success' => 'Photo has Been deleted');
            }
        }
        
        //************************************************************************************************************************************
        //***** DELETE IMAGES
        $dltimg = Image::find("foldername='".$albumname."'");
        $data = array('error' => 'Not Found');
        if ($dltimg) {
            if($dltimg->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
         echo json_encode($data);
        
    }




}

