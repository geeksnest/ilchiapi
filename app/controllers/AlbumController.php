<?php

namespace Controllers;

use \Models\Album as Album;
use \Models\Image as Image;

class AlbumController extends \Phalcon\Mvc\Controller {
 public function createNewsAction()
 {

   $data = array(); 
   if ($_POST)
   {
    $status = 0;

    if($_POST['check'])
    {
        $status = 1;
    }
    else
    {
     $status = 0;
 }
 $news = new News();
 $news->assign(array(
    'title' => $_POST['title'],
    'body' => $_POST['body'],
    'banner' => $_POST['banner'],
    'status' => $status,
    'date' => date('Y-m-d')
    ));
 if (!$news->save()) 
 {
    $data['error'] = "Something went wrong saving the data, please try again.";

} 
else 
{

    $newsid = $news->newsid;
    $tags = array(); 
    $tags = $_POST['tags'];
    foreach($tags as $tag){
        $newscategory = new Newscategory();
        $newscategory->assign(array(
            'newsid' => $newsid,
            'categoryid' => $tag 
            ));
        if (!$newscategory->save()) 
        {
            $data['error'] = "Something went wrong saving the data, please try again.";

        } 
        else 
        {
         $data['success'] = "Success";
     }
 }




 $data['success'] = "Success";
}
}
echo json_encode($data);




}


public function managenewsAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = News::find();
    } else {
        $conditions = "firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%'";
        $Pages = News::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'newsid' => $m->newsid,
            'title' => $m->title,
            'status' => $m->status
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}


public function managecategoryAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Categorynames::find();
    } else {
        $conditions = "firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%'";
        $Pages = Categorynames::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'id' => $m->id,
            'categoryname' => $m->categoryname,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}



public function listcategoryAction()
{

    $getcategory= Categorynames::find(array("order" => "id ASC"));
    foreach ($getcategory as $getcategory) {;
        $data[] = array(
            'id'=>$getcategory->id,
            'categoryname'=>$getcategory->categoryname
            );
    }
    echo json_encode($data);

}

public function newsdeleteAction($newsid) {
    $conditions = "newsid=" . $newsid;
    $news = News::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'News Deleted');
        }
    }
    echo json_encode($data);
}


public function categorydeleteAction($id) {
    $conditions = "id=" . $id;
    $news = Categorynames::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'Category Deleted');
        }
    }
    echo json_encode($data);
}


public function newsinfoAction($newsid) {

    $news = News::findFirst("newsid=" . $newsid);
    $data = array();
    $catdata = array();
    if ($news) {
        $getcategory= Newscategory::find("newsid=" . $newsid);
        foreach ($getcategory as $getcategory) {;
            $catdata[] = array(
                'newsid'=>$getcategory->newsid,
                'categoryid'=>$getcategory->categoryid
                );
        }
        $data = array(
            'newsid' => $news->newsid,
            'title' => $news->title,
            'body' => $news->body,
            'banner' => $news->banner,
            'check' => $news->status,
            'catdata' => $catdata
            );
    }

    echo json_encode($data);

}




public function newsUpdateAction() 
{

    $data = array();
    if ($_POST) {
        $status = 0;

        if ($_POST['check'] == true) {
            $status = 1;
        } else {
            $status = 0;
        }

        $newsid = $_POST['newsid'];
        $news = News::findFirst('newsid=' . $newsid . ' ');
        $news->title = $_POST['title'];
        $news->body = $_POST['body'];
        $news->banner = $_POST['banner'];
        $news->status = $status;

        if (!$news->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {

           $conditions = "newsid=" . $newsid;
           $newscat = Newscategory::find(array($conditions));
           $data = array('error' => 'Not Found');
           if ($newscat) {
            if ($newscat->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }



        $tags = array(); 
        $tags = $_POST['tags'];
        foreach($tags as $tag){
            $newscategory = new Newscategory();
            $newscategory->assign(array(
                'newsid' => $newsid,
                'categoryid' => $tag 
                ));
            if (!$newscategory->save()) 
            {
                $data['error'] = "Something went wrong saving the data, please try again.";

            } 
            else 
            {
             $data['success'] = "Success";
         }

     }

     $data['success'] = "Success";
 }
}
echo json_encode($data);
}


public function categoryUpdateAction() 
{

    $data = array();
    $id = $_POST['id'];
    $news = Categorynames::findFirst('id=' . $id . ' ');
    $news->categoryname = $_POST['catnames'];

    if (!$news->save()) 
    {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } 
    else 
    {
        $data['success'] = "Success";
    }
    echo json_encode($data);
}




public function createcategoryAction()
{

   $data = array(); 

   $catnames = new Categorynames();
   $catnames->assign(array(
    'categoryname' => $_POST['catnames'],
    ));
   if (!$catnames->save()) 
   {
    $data['error'] = "Something went wrong saving the data, please try again.";
} 
else 
{
 $data['success'] = "Success";
}
echo json_encode($data);
}



public function showAlbumAction($offset) {

    $album = Album::find(array("order"=>"id DESC"));
    $data = array();
    $data2 = array();

    foreach ($album as $album) {
       $data2 = array();
       $folder = Image::find("folderid='".$album->album_id."'");
       foreach ($folder as $folder) {
        $data2[] = array(
            'id' => $folder->id,
            'generateid' => $folder->generateid,
            'description' => $folder->description,
            'path' => $folder->path,
            'title' => $folder->title,
            'foldername' => $folder->foldername,
            'folderid' => $folder->folderid
            );
    }

    $data[] = array(
        'id' => $album->id,
        'album_name' => $album->album_name,
        'description' => $album->description,
        'album_id' => $album->album_id,
        'date' => $album->date,
        'cat' => count($data2),
        'cats' => reset($data2)

        );
}

$albuminfo = json_encode($data, JSON_NUMERIC_CHECK);
echo $albuminfo;
}

public function folderAction($id) {

    $album = Album::findfirst("album_id='".$id."'");
    $data = array();
    $folder = Image::find("folderid='".$id."'");
    foreach ($folder as $folder) {
        $data[] = array(
            'id' => $folder->id,
            'generateid' => $folder->generateid,
            'description' => $folder->description,
            'path' => $folder->path,
            'title' => $folder->title,
            'foldername' => $folder->foldername,
            'folderid' => $folder->folderid,
            'description' => $album->description
            );
    }
    $folderinfo = json_encode($data);
    echo $folderinfo;
}






}
