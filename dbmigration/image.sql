-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 09:27 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
`id` int(11) NOT NULL,
  `generateid` text NOT NULL,
  `description` text,
  `path` text NOT NULL,
  `title` text,
  `foldername` text NOT NULL,
  `folderid` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `generateid`, `description`, `path`, `title`, `foldername`, `folderid`) VALUES
(1, 'a8efc69b908dce277b26c043b6b8cc51', 'Description Here', 'e0189b49e9e286d79da4434e3d92a693.jpg', 'Title Here', 'Brain Art Festival August 15, 2009', '320da064f80cc3d3d10c63f543d63aaf'),
(2, 'de6577f5a24eee4401373c7e02164230', 'Description Here', 'be14e759b9668c67f9868759cc6d96d5.jpg', 'Title Here', 'Brain Art Festival August 15, 2009', '320da064f80cc3d3d10c63f543d63aaf'),
(3, '576d2d790d78077bbde3bf8d913119d6', 'Description Here', '68ed757b3a62065b2166d6c06431f40f.jpg', 'Title Here', 'Brain Art Festival August 15, 2009', '320da064f80cc3d3d10c63f543d63aaf'),
(4, '5512c546c1793abe3ee2271390deb6ed', 'Description Here', '657081a902e9d92e1a4233f6c4ac5242.jpg', 'Title Here', 'Brain Art Festival August 15, 2009', '320da064f80cc3d3d10c63f543d63aaf'),
(5, '342370efbfbfd1364fcc72c51fcbb437', 'Description Here', '83b0d6679d6234bcd8d8550c782921be.jpg', 'Title Here', 'Brain Art Festival August 15, 2009', '320da064f80cc3d3d10c63f543d63aaf'),
(6, 'edaec02b1998a196fd0592e546aa2463', 'Description Here', '6f8c3bd82f8a9845ca41b11128672750.jpg', 'Title Here', 'Brain Art Festival August 15, 2009', '320da064f80cc3d3d10c63f543d63aaf'),
(8, '57a16c2541ca352c3405ba0733fe9888', 'Description Here', '8ce44c65c945c90fc4cef7db43eac4c3.jpg', 'Title Here', 'A Healing Day with Nature', '0665ea070121aa35912a1903b2a888ae'),
(9, 'eabaec6300a6c763c8248b16a893b512', 'Description Here', '1ffb18b7050344c1d13a768e2e567810.jpg', 'Title Here', 'A Healing Day with Nature', '0665ea070121aa35912a1903b2a888ae'),
(10, 'ac082bf653bd99dc181b901ff41e5c2c', 'Description Here', 'd0bcf32195321873e35fb80525f10e27.jpg', 'Title Here', 'A Healing Day with Nature', '0665ea070121aa35912a1903b2a888ae'),
(11, 'a673b964567c10647c451bbd441f87fd', 'Description Here', '821a31d6fde3183cdd70addab94a3cfd.jpg', 'Title Here', 'A Healing Day with Nature', '0665ea070121aa35912a1903b2a888ae'),
(12, 'dc2c57c856ddf988eeedc5b8b2eac4ee', 'Description Here', '6f1e48769ce423a6bf86224882ad3f1f.jpg', 'Title Here', 'A Healing Day with Nature', '0665ea070121aa35912a1903b2a888ae'),
(13, 'e1b94bf7512ddd32c41500450483aac0', 'Description Here', 'e1ecd46e92070d8cf8408ad5bb9b57d4.jpg', 'Ilchi-Lee_The-Call-of-Sedona_2', 'The Call of Sedona Launch', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(14, '3db8a7e43d569c6c32a2e6b1792be9fd', 'Description Here', 'ee1cae231f96d0bccf7e65c6a75e46ac.jpg', 'Title Here', 'The Call of Sedona Launch', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(15, 'eee53a2be9f8bc8cc48d6df4aab97836', 'Description Here', 'be7909556f033bfcfefe26d2acfee79d.jpg', 'Title Here', 'The Call of Sedona Launch', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(16, '1f538604c6ba527ed10e172eec312020', 'Description Here', 'e99859c5ef76ac29b49670bfee9f837f.jpg', 'Title Here', 'The Call of Sedona Launch', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(17, 'b0d9d16b4956d4eee51853eff5c2468d', 'Description Here', 'bdef831b827a5ca43d3e76f77f99addd.jpg', 'Title Here', 'The Call of Sedona Launch', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(18, '58d445289686cfd79d67a265fdf3119f', 'Description Here', '9579867814832bd2ef76f75f73d8c9f1.jpg', 'Title Here', 'The Call of Sedona Launch', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(19, '8f8a308d4de2d2579950a544495ce7ed', 'Description Here', '360e02128a5020acbb31a0f17f3d4366.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(20, 'f7c263a9fc23302041ab24f4da3d0007', 'Description Here', 'f00f7722b8e51c3c99e46058edfb22a5.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(21, 'c88ffc1f4a83ddbebde5fa02e1ec8161', 'Description Here', '7430eb342c67e5cfde86ede0ea47fb0c.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(22, 'cff79fc6bcbca8091e1b280ce502b5cb', 'Description Here', '7eaa323444c6cb32418921227f26ba5e.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(23, '2eee0087573f0d3cf20273e0e94f5e52', 'Description Here', '36df51d0798ef3498e7fac6bf38865bc.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(24, '5afb710bcddfc4edf636ebc2761e0b4c', 'Description Here', '26e9e29ceb585cb4d70f101b258a5551.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(25, '6fb420fac88c646b5407291a5ea93369', 'Description Here', '01b42adabac8405378dcceb33f389d58.png', 'Title Here', 'Brain Art Festival August 15, 2009', '2df06c53b8d66dadfe3e7899ef4f194b'),
(26, 'eec7b237049c6411b9ab8639b13ac8cb', 'Description Here', '3a218b7c8216e0f87bc617d972ab3d23.jpg', 'Title Here', 'BR Circulation School', '7e0561d3a0c4c6c7b98d908d6bac6910'),
(27, '6cec84e832d90e4a3401293ad353c614', 'Description Here', '0107f5678c0d84e04f3b05cfb770d329.jpg', 'Title Here', 'BR Circulation School', '7e0561d3a0c4c6c7b98d908d6bac6910'),
(28, '5dd2fe208d362e8b767b5a82b8f08aca', 'Description Here', '4081f781024fad6011b1d2f3d60acab6.jpg', 'Title Here', 'BR Circulation School', '7e0561d3a0c4c6c7b98d908d6bac6910'),
(29, 'bfd48eb0d1ecab3f0904b9439e5d185c', 'Description Here', 'be542d9393d4edc0b30a8de6aa3137ee.jpg', 'Title Here', 'BR Circulation School', '7e0561d3a0c4c6c7b98d908d6bac6910'),
(30, 'cfdb8a1628eda9a53ff01026784ccecd', 'Description Here', '6fbfbfc81a5738714c0483c3fd601f7c.jpg', 'Title Here', 'BR Circulation School', '7e0561d3a0c4c6c7b98d908d6bac6910');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image`
--
ALTER TABLE `image`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
