-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 09:27 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `featuredphoto`
--

CREATE TABLE IF NOT EXISTS `featuredphoto` (
`id` int(11) NOT NULL,
  `path` text NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `featuredphoto`
--

INSERT INTO `featuredphoto` (`id`, `path`, `title`) VALUES
(14, '0bef86a2dddee7c81f0a9f5245995fd5.jpg', 'Title Here'),
(15, '11d413917c8812fdf5be1069ac59d7e8.jpg', 'Title Here'),
(17, '5ba787eef52bda0d999e179ca20f6b5f.jpg', 'Title Here'),
(18, 'b4523e213b0d2e51dfcd4f165f2bcdd7.PNG', 'Title Here');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `featuredphoto`
--
ALTER TABLE `featuredphoto`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `featuredphoto`
--
ALTER TABLE `featuredphoto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
