-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2015 at 03:00 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE IF NOT EXISTS `testimonial` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `comporg` text NOT NULL,
  `message` text NOT NULL,
  `status` text NOT NULL,
  `publish` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `email`, `comporg`, `message`, `status`, `publish`) VALUES
(2, 'dfasd', 'dsafdsf', 'dsf', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures', 'fsdafsdf', 'publish'),
(3, 'adsfsadf', 'sdfsadf', 'dsfdsa', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'dafsadf', 'publish'),
(4, 'dsafsda', 'fsdaf', 'dsafsda', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'dsafsd', 'publish'),
(5, 'kyben', 'kc.deguia@gmail.com', '2134234', 'sdfsdf', 'stat', 'publish'),
(6, 'Kyben', 'flipt_silent01@yahoo.com', 'asdfasdfdsaf', 'Registration\nBECOME AN EARTH CITIZEN BY REGISTERING AND MAKING A DONATION\n\nAs a responsible citizen of the Earth, I agree to develop my body, mind, and spirit to their greatest capacity and use them to better the life that I share with others both locally and globally.\n\nAs an honorable citizen of the Earth, I agree to demonstrate the highest human virtues and the greatness of the human spirit in order to encourage and inspire others to manifest their best for the benefit of all.\n\nAs a caring citizen of the Earth, I agree to practice mindful living to create positive changes in my lifestyle and work with my fellow Earth Citizens to create a peaceful sustainable world.', 'stat', 'publish'),
(7, 'burn', 'efrenbautistajr@gmail.com', 'asdf', 'sdf', 'stat', 'publish'),
(8, 'Kmaikazee', 'kamikazee@gmail.com', 'OPM', 'Tagpuan', 'stat', 'publish'),
(9, '123213', 'flipt_silent01@yahoo.com', 'asdfsd', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'stat', 'publish'),
(11, 'asdfsadf', 'flipt_silent01@yahoo.com', 'sadfsadf', 'sdafsdfsdafsadf', 'stat', 'publish'),
(13, 'asdfsdaf', 'flipt_silent01@yahoo.com', 'asdf fg assadfg sdfgsdf', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'stat', 'publish'),
(14, 'burn', 'rainierllarenas@gmail.com', 'asdfasdfdsaf', 'sadfsdafsdafsadfsad sad sad dsfsdaf sdf sdaf', 'stat', 'unpublish'),
(15, 'asdfsdafsadfsdf', 'flipt_silent01@yahoo.com', 'sadf', 'asdfsdafsadf', 'stat', 'unpublish'),
(16, 'sdafas', 'ecoadmin@sdfsd.sdf', 'sadfsdaf', '#sadfsadsdaf', 'stat', 'unpublish'),
(17, 'Kyben', 'flipt_silent01@yahoo.com', 'asdfasdf', 'We are a non-profit organization and movement who believes that now is the time that humanity needs to respect and care for the Earth. In a world where conflicts between nations, religions, ethnicities, cultures, and ideologies have become intractable, humanity urgently needs to rise above these small problems and conflicts to become one in our concern for the Earth. Peace will come when we collectively recognize that the Earth''s fate will determine humanity''s fate, and we come to hold the Earth as our shared concern and value.', 'stat', 'unpublish');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
