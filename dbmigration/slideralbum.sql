-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2015 at 11:52 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `slideralbum`
--

CREATE TABLE IF NOT EXISTS `slideralbum` (
`id` int(11) NOT NULL,
  `album_name` text NOT NULL,
  `album_id` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slideralbum`
--

INSERT INTO `slideralbum` (`id`, `album_name`, `album_id`, `active`) VALUES
(7, 'test', '123124', 0),
(8, 'this is a image', 'f002cde8bd11588776e43f22ef4fd9c3', 0),
(9, 'test', 'ca8893c68cc971ef7db84012f873fdf7', 0),
(10, '123', 'ae160357e2e25d5c74aec37a22ddc38c', 0),
(11, '12', 'b04b65ddbe1450e5b4e86c5a3079bffb', 0),
(12, '1', '35f9f10ef80f209cfd3f9dcf0a5bf45d', 0),
(13, '2', '731e0c315202e39f66cfbd047f65e607', 0),
(14, '12', 'e458e001f8033cb93f62befbda10b096', 0),
(15, 'asdfsda', '94688c9b762e03c624f264196ff5af03', 0),
(16, 'test', '462b0b789633ea11d8c7b67e0787ac50', 0),
(17, 'test', '83446c8c8ac16e96b7a54303d294abe0', 0),
(18, '12', '8d7beebecb390290ce8afba8c09d718b', 0),
(19, 'wew', '9d5e7bd1aa7c2782f359ec6b798c1404', 0),
(20, '1', '3f2295d2ab330f6991852e13d1f67295', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `slideralbum`
--
ALTER TABLE `slideralbum`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `slideralbum`
--
ALTER TABLE `slideralbum`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
