-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 09:28 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `newsimage`
--

CREATE TABLE IF NOT EXISTS `newsimage` (
`id` int(11) NOT NULL,
  `generateid` text NOT NULL,
  `description` text,
  `path` text NOT NULL,
  `title` text
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsimage`
--

INSERT INTO `newsimage` (`id`, `generateid`, `description`, `path`, `title`) VALUES
(2, '8c72be93c781d43c00dee15ed56cfdfd', 'Description Here', '5e377ede73a527cdd24694b47ac5f1c7.jpg', 'Title Here'),
(3, 'bfd1a10ad13617d4490d574d1f0b1f5b', 'Description Here', '035eefa27acb56a883e72f39fa4c7606.png', 'Title Here'),
(4, '703565d43bd820e63c4ba73ceab4adde', 'Description Here', '05ad119e9fc5e177a83b62cbb18ce3f1.jpg', 'Title Here'),
(5, '37bba0b6d20b7f2a2d7baf6ffd7aa948', 'Description Here', 'bba92995fa012e68c993b8899511a836.PNG', 'Title Here');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `newsimage`
--
ALTER TABLE `newsimage`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `newsimage`
--
ALTER TABLE `newsimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
