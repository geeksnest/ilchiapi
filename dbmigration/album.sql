-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 09:27 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
`id` int(11) NOT NULL,
  `album_name` text NOT NULL,
  `description` text NOT NULL,
  `album_id` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `album_name`, `description`, `album_id`) VALUES
(5, 'The Call of Sedona Launch', 'The launch of my latest book, The Call of Sedona: Journey of the Heart', '0f00976e5c6bb8a4c54e1ec9c67df5c7'),
(6, 'Brain Art Festival August 15, 2009', 'On Saturday, August 15, 2009, I gave an interactive performance at the Brain Art Festival at Radio City Music Hall in New York City. At the festival I demonstrated how each of us can become a brain artist--a person who uses their brainâ€™s vast creative power to make their life a work of art by living to achieve the transformation of their soul and hoping to leave a legacy that contributes to a positive and empowering world vision', '2df06c53b8d66dadfe3e7899ef4f194b'),
(7, 'BR Circulation School', 'Ilchi Lee taught Brain Education to a small group of students on Jeju Island in South Korea in March 2008. He led them in several different types of meditation exercises', '7e0561d3a0c4c6c7b98d908d6bac6910');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
