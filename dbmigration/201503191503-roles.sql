-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 19, 2015 at 05:59 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `roleCode` varchar(200) NOT NULL,
  `roleDescription` varchar(255) NOT NULL,
  `rolePage` varchar(255) NOT NULL,
  `roleGroup` varchar(255) NOT NULL,
  PRIMARY KEY (`roleCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`roleCode`, `roleDescription`, `rolePage`, `roleGroup`) VALUES
('calendarrole', 'Calendar', 'addcalendar,viewcalendar,editcalendar', 'calendar'),
('contactsrole', 'Contacts', 'managecontacts', 'contacts'),
('imagegalleryrole', 'Image Gallery', 'index,image,manage_album,edit_album', 'gallery'),
('newslettersrole', 'News Letter', 'addsubscriber,subscriberslist,editsubscriber,createnewsletter,managenewsletter, editnewsletter, sendnewsletter', 'subscribers'),
('newsrole', 'News', 'createnews, managenews, editnews, category,tags', 'news'),
('pagesrole', 'Pages', 'managepages,createpage,editpage', 'pages'),
('projectsrole', 'Projects', 'createfeaturedproject,managefeaturedproject,editfeaturedproject', 'featuredprojects'),
('publicationrole', 'Publications', 'index,publication,manage,editpub', 'publication'),
('slidergalleryrole', 'Slider Images', 'index,slider,slider_lists', 'slider'),
('usersrole', 'Users', 'create, createSave, list, members', 'users'),
('videogalleryrole', 'Video Gallery', 'index,videos,video_lists,edit', 'video');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

