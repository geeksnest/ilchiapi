-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2015 at 04:51 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbeco`
--

-- --------------------------------------------------------

--
-- Table structure for table `sentlogs`
--

CREATE TABLE IF NOT EXISTS `sentlogs` (
`id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `newsletterid` int(11) NOT NULL,
  `newslettertitle` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sentlogs`
--

INSERT INTO `sentlogs` (`id`, `email`, `newsletterid`, `newslettertitle`) VALUES
(1, 'rainllarenas@gmail.com', 1, 'aaaaaaaaaaaa'),
(2, 'llarenasjanrainier@gmail.com', 1, 'aaaaaaaaaaaa'),
(3, 'llarenasjanrainier@gmail.com', 2, 'bbbbbbbbbbb');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sentlogs`
--
ALTER TABLE `sentlogs`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sentlogs`
--
ALTER TABLE `sentlogs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
