-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2015 at 11:52 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `sliderimage`
--

CREATE TABLE IF NOT EXISTS `sliderimage` (
`id` int(11) NOT NULL,
  `generateid` text NOT NULL,
  `description` text NOT NULL,
  `path` text NOT NULL,
  `title` text NOT NULL,
  `foldername` text NOT NULL,
  `folderid` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliderimage`
--

INSERT INTO `sliderimage` (`id`, `generateid`, `description`, `path`, `title`, `foldername`, `folderid`) VALUES
(21, 'a39c01346f79be7b3d0a5cbb829f8b8f', 'Description Here', '645dda8862788e8a43803235c2b2c4e0.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(22, '0f76289b57ea0ff706cd9ca9edbd22e5', 'Description Here', '3f3ab3f8ed98763a3e85ebe29cbd0620.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(23, '2f9ad2a332143960e873e114e75a5cd2', 'Description Here', '44ed94d4aca7e79870995d5227346760.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(24, '72340183af6c3df7f72da6fff723b307', 'Description Here', '21016d7be1f444c20d64cd65d3d075c3.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(25, 'd3666e2dd3cb63496d7d709bb99c4d22', 'Description Here', '4d516495f4444b2ad40c142aa5552548.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(26, '34875a8da2bab1862e1379a6a47205af', 'Description Here', '23b2f9a2b79d88000be63f9f9e3eea95.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(27, '1d3744c2ee6fd0aba603d12d7fcc34bd', 'Description Here', '0b491b170444ad4883175681a2e94e85.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(28, '98c464121afcc62c1e1dd66c5ca498b4', 'Description Here', '0048ac98064f031da527238c1fde9b4a.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(29, '873afc69453b73e2e6f98012cb4c4097', 'Description Here', 'd6fbb32c85661b5f573b3afb3b540e59.png', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(30, '438b6c24b8bd22a82261aeb677e51e55', 'Description Here', 'a2fcf50b8c275d97114e53f3c8c13491.png', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(31, 'b3b81cba02441fbb4c8fb8ae50d4d42d', 'Description Here', '60a2fd0c048c4895060bc6eae2f6ee2f.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(32, 'be6198e73176124b7efe6089e94744cf', 'Description Here', '209e37a38de3e4197ccaa1f9faa11e32.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(33, 'f3daa30cbbf1b93cd9db25faede396b1', 'Description Here', 'a1c4afa966e0f0f959f4b30b1e2fe30a.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(34, 'd995e61b8043d02070d76ce373e85538', 'Description Here', '4cf87777f5475ac86da4bf4e7fcd831b.jpg', 'Title Here', 'undefined', '3f2295d2ab330f6991852e13d1f67295'),
(35, '7b444c89af3ae30edc05369df19bd646', 'fg', '21637481bc3af05800d07863b88d0f0e.jpg', 'sdaf', 'undefined', '3f2295d2ab330f6991852e13d1f67295');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sliderimage`
--
ALTER TABLE `sliderimage`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sliderimage`
--
ALTER TABLE `sliderimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
