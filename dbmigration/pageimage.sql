-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 09:28 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `pageimage`
--

CREATE TABLE IF NOT EXISTS `pageimage` (
`id` int(11) NOT NULL,
  `generateid` text NOT NULL,
  `description` text,
  `path` text NOT NULL,
  `title` text
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pageimage`
--

INSERT INTO `pageimage` (`id`, `generateid`, `description`, `path`, `title`) VALUES
(1, '8b764650e9ddca45915faf344a1e4170', 'Description Here', '431a93484fb9f83e4b678a1db1ca54ee.jpg', 'Title Here'),
(2, '4849e9ee0b8a6794afb5c78afe6b37ad', 'Description Here', '30524aa5eed2b505b509f5b709a04165.png', 'Title Here'),
(3, 'afe868e04ecca7cc1104879f8106d8bc', 'Description Here', '2fdb901f3edd14733387ed1b30a55386.png', 'Title Here'),
(4, 'b2a3f3db309c8f2c59327f22652d7a01', 'Description Here', '13a4c65706550e623c1f031e05217c78.png', 'Title Here'),
(5, '72aeb246bec7084e31144673e26f3b25', 'Description Here', 'a8bfcac0527a835a0f7d013a37cd622c.png', 'Title Here'),
(6, 'cf3ea574fa9c8fa62d3f39967bbc78ce', 'Description Here', 'b3b3d8552391fa8c94716c1c5065d926.png', 'Title Here'),
(7, 'c831ea19d55544bdf833664e3ffe442e', 'Description Here', 'ee83be61fa1ecbe41f2c8c3d236fcebb.png', 'Title Here'),
(8, 'd8583c5aad4764d813d647f6c3a48c6d', 'Description Here', 'edd159b66783e6aa3759125c4f425165.png', 'Title Here'),
(9, 'ed00caf822781c5e98163ae5eabb6ba0', 'Description Here', '46c7489177070aa4f33b89633083fed1.png', 'Title Here'),
(10, '293e14bfe2e1717702f8fe92fa1027e5', 'Description Here', '2113004668150d1a75d40e8670a23f38.jpg', 'Title Here'),
(11, '9cabdd01baa29bd6e46ed63565cbc0b9', 'Description Here', '0ad618acf18476ed3e0ce94bf09d1e94.jpg', 'Title Here'),
(12, 'fa57f4b4f486b1048a77adbbe462f2c8', 'Description Here', '949be40310b2eea5c1fa28270ccdb216.jpg', 'Title Here');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pageimage`
--
ALTER TABLE `pageimage`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pageimage`
--
ALTER TABLE `pageimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
