-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 09:27 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `featuredprojects`
--

CREATE TABLE IF NOT EXISTS `featuredprojects` (
`feat_id` int(11) NOT NULL,
  `feat_title` varchar(150) NOT NULL,
  `feat_content` text NOT NULL,
  `feat_picpath` varchar(350) NOT NULL,
  `feat_status` int(11) NOT NULL,
  `feat_pub` int(11) NOT NULL,
  `pubtxt` text NOT NULL,
  `feat_loc` int(11) NOT NULL,
  `feat_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `featuredprojects`
--

INSERT INTO `featuredprojects` (`feat_id`, `feat_title`, `feat_content`, `feat_picpath`, `feat_status`, `feat_pub`, `pubtxt`, `feat_loc`, `feat_date`) VALUES
(12, 'Earth Citizen Movement', '<h1>Earth Citizens Movement</h1>\n\n<p>&nbsp;</p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">Big changes begin with small changes within each person and end when many people concerned with change come together to make the world better. Many people who practice Ilchi Lee&rsquo;s personal growth methods experience an evolution in consciousness and come to want to help empower others and the planet to thrive. They have come together in movements initiated and/or inspired by Ilchi Lee.</span></p>\n\n<p>&nbsp;</p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">The current largest and most active movement is the Earth Citizen Movement, which promotes the Earth Citizen ideal. Being an Earth Citizen means recognizing you are a &ldquo;citizen&rdquo; of the Earth and choosing to make that identity more important than your identity as a member of a particular ethnic group, religion, or nation. This identity brings all people together as one with each other and with the Earth because the Earth is the largest common denominator among all human beings. We are all completely dependent on the Earth.</span></p>\n\n<p>&nbsp;</p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">Because of our dependency, the Earth Citizen ideal says that we should act as stewards of the earth and preserve its bounty to sustain our lives and all life on the planet. In order to do this, we need to manage the Earth as a whole rather than fighting for pieces of it among ourselves. Ilchi Lee calls this Earth Management.</span></p>\n\n<p>&nbsp;</p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">The Earth Citizen Movement promotes peace and conservation, but it is not a political or an environmental movement. Its aim is to raise awareness and acceptance of the Earth Citizen ideal and to teach and promote an Earth Citizen culture. It expects that the practice of this culture by enough people on the planet will lead to a new way of being, thinking, and doing that will lead to Earth Management and health, happiness, and peace for all.&nbsp;</span></p>\n\n<p>&nbsp;</p>\n\n<table border="0" cellpadding="1" cellspacing="1" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(102, 102, 102); font-family:arial,helvetica,sans-serif; font-size:12px; line-height:22px; max-width:100%; width:950px">\n	<tbody>\n		<tr>\n			<td>\n			<p><span style="color:black; font-family:arial; font-size:16pt">Get Involved</span></p>\n\n			<p>&nbsp;</p>\n\n			<p><span style="color:black; font-family:arial; font-size:10pt">The organization spearheading the Earth Citizen Movement is the Earth Citizens Organization (ECO), a non-profit that&nbsp;was established by Ilchi Lee to help people create changes in their lives to support peace and sustainability locally and globally.</span></p>\n\n			<p>&nbsp;</p>\n\n			<p><span style="color:black; font-family:arial; font-size:10pt">Earth Citizens Organization (ECO) is a 501(c)(3) nonprofit that promotes natural health and</span></p>\n\n			<p><span style="color:black; font-family:arial; font-size:10pt">mindful living for a sustainable world. The name of the organization represents the understanding that we are all citizens of the Earth, and as such, share a common responsibility as its constituents for the well-being and vitality of communities of all life forms on our home planet. &ldquo;Earth Citizen&rdquo; is the shared identity for all people who take this responsibility, and ECO is the entity that advances this spirit of Earth Citizenship. &nbsp;</span></p>\n			</td>\n			<td><img alt="" src="http://ilchiapi/images/sliderimages/edd159b66783e6aa3759125c4f425165.png" style="border:0px; box-sizing:border-box; height:142px; vertical-align:middle; width:202px" /></td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">ECO offers a very unique and simple yet powerful and practical approach to transformation both for the individual and the world: &ldquo;think soft.&rdquo; Rather than directly spearheading fixed systems, corporations, institutions and dogmas, first change more immediately malleable things such as the way we breathe, eat, manage stress, and think about people and other sentient beings. These &ldquo;soft&rdquo; aspects directly shape our behaviors and our choices, both on a personal and a global scale. In this respect, they are fundamental&mdash;more so than any material modification. These elements, together with training and practices for mindfulness and leadership, are the foundation of the training programs sponsored by ECO. They are also the groundwork of the &ldquo;Earth Citizen Movement&rdquo; to create a domino effect of conscious change in the lives of every human being.</span></p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">ECO&rsquo;s activities include providing education and training for: leadership development, personal management for healthier lifestyle, and cross-cultural communion for balanced and harmonious living throughout the world. To make this training and education accessible to a wider demographic, ECO not only accepts students of all age groups but also plans to develop training facilities in multiple locations throughout the U.S. and also online education services. Currently, the primary training location is in Sedona, Arizona.</span></p>\n\n<p><span style="color:black; font-family:arial; font-size:10pt">For more information about how to participate, visit <a href="http://www.earthcitizens.org">http://www.earthcitizens.org</a>.</span></p>\n', '0bef86a2dddee7c81f0a9f5245995fd5.jpg', 1, 1, 'Publish', 2, '2015-03-04'),
(13, 'The Change Project', '<p><img alt="" class="alignleft size-full wp-image-4929" src="http://www.ilchi.com/wp-content/uploads/2011/09/CHANGE_Project_icon.png" style="background-color:rgb(254, 249, 249); border:0px; color:rgb(54, 54, 54); display:inline; float:left; font-family:arial; height:125px; line-height:20px; margin:0px 14px 2px 0px; padding:0px; vertical-align:middle; width:140px" title="CHANGE_Project_icon" /><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">In the spring of 2013, Ilchi Lee began taking part in a project for personal and global change that he inspired&mdash;the Change Project. Encompassing a screening and lecture tour, documentary film,&nbsp;</span><a href="http://www.ilchi.com/books-cds/books/" style="margin: 0px; padding: 0px; color: rgb(54, 54, 54); text-decoration: none; font-family: Arial; line-height: 20px; background-color: rgb(254, 249, 249);">two books</a><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, and website, the Change Project is spreading the understanding that a change in perspective can substantially change your life. It aims to help people shift from a slave to change to an agent of change. It says we are all made of the same elementary particles&mdash;what Ilchi Lee calls&nbsp;</span><a href="http://www.ilchi.com/teachings/energy-principle/#LifeParticles" style="margin: 0px; padding: 0px; color: rgb(54, 54, 54); text-decoration: none; font-family: Arial; line-height: 20px; background-color: rgb(254, 249, 249);">LifeParticles</a><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">. Our mind can transform LifeParticles into anything we desire with our relaxed focus and targeted action. Knowing this brings confidence, self-worth, and hope for the individual and society.</span><br />\n<a href="http://www.changeyourenergy.com/" style="margin: 0px; padding: 0px; color: rgb(0, 114, 188); text-decoration: none; font-family: Arial; line-height: 20px; font-weight: bold; background-color: rgb(254, 249, 249);" target="_blank">http://www.ChangeYourEnergy.com</a></p>\n', '11d413917c8812fdf5be1069ac59d7e8.jpg', 0, 1, 'Publish', 2, '2015-03-04'),
(14, 'The Call of Sedona', '<p><img alt="" class="alignleft size-full wp-image-4541" src="http://www.ilchi.com/wp-content/uploads/2011/09/CallofSedona_NYTimes_Small.jpg" style="background-color:rgb(254, 249, 249); border:0px; color:rgb(54, 54, 54); display:inline; float:left; font-family:arial; height:125px; line-height:20px; margin:0px 14px 2px 0px; padding:0px; vertical-align:middle; width:140px" title="CallofSedona_NYTimes_Small" /><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">In September of 2008, Ilchi Lee published a memoir-like book titled&nbsp;</span><em>The Call of Sedona: Journey of the Heart</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">. With his inspirational descriptions of his meditative revelations in Sedona, Arizona&mdash;a land of breathtaking beauty and spiritual attraction&mdash;he demonstrated a way for readers to live their own journey. The book reached the&nbsp;</span><em>New York Times</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">&nbsp;best seller list as well as the best seller lists of&nbsp;</span><em>USA Today</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, the&nbsp;</span><em>LA Times</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, and the&nbsp;</span><em>Washington Post</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">. In April 2012, the world rights were purchased by Scribner, an imprint of Simon &amp; Schuster. Scribner published its print and electronic editions in July of 2012, while Simon &amp; Schuster Audio published an audio edition. Ilchi Lee was engaged in an international book tour for&nbsp;</span><em>The Call of Sedona</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">&nbsp;for much of 2012.</span><br />\n<a href="http://www.callofsedona.com/" style="margin: 0px; padding: 0px; color: rgb(0, 114, 188); text-decoration: none; font-family: Arial; line-height: 20px; font-weight: bold; background-color: rgb(254, 249, 249);" target="_blank">http://www.callofsedona.com</a></p>\n', '5ba787eef52bda0d999e179ca20f6b5f.jpg', 0, 1, 'Publish', 2, '2015-03-04'),
(15, 'The Miracle the World Is Waiting For', '<p><img alt="Ilchi Lee - The Miracle the World is Waiting For" class="alignleft size-full wp-image-3581" src="http://www.ilchi.com/wp-content/uploads/2011/12/miracle2012_thumb.jpg" style="background-color:rgb(254, 249, 249); border:0px; color:rgb(54, 54, 54); display:inline; float:left; font-family:arial; height:125px; line-height:20px; margin:0px 14px 2px 0px; padding:0px; vertical-align:middle; width:140px" title="Ilchi Lee - The Miracle the World is Waiting For" /><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">In Ilchi Lee&rsquo;s e-book,&nbsp;</span><em>The Miracle the World Is Waiting For</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, he reminds us that our lives are composed of our choices. He writes, &ldquo;I&rsquo;ve made the choice to make 2012 the most hopeful year in the history of humanity &hellip; instead of wondering what will happen in the year 2012, or feeling anxious about it, wouldn&rsquo;t it be more productive for us to plan together the kind of year we want 2012 to be, and then create it?&rdquo; That choice began on January 1, 2012, when the energy of the world shifted and it became easier for everyone to experience awakening, connection, and oneness like never before. Click the link below to find out what you can do to take the future into your hands.</span><br />\n<a href="http://www.ilchi.com/miracle2012" style="margin: 0px; padding: 0px; color: rgb(0, 114, 188); text-decoration: none; font-family: Arial; line-height: 20px; font-weight: bold; background-color: rgb(254, 249, 249);" target="_blank">http://www.ilchi.com/miracle2012</a></p>\n', 'b4523e213b0d2e51dfcd4f165f2bcdd7.PNG', 0, 1, 'Publish', 2, '2015-03-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `featuredprojects`
--
ALTER TABLE `featuredprojects`
 ADD PRIMARY KEY (`feat_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `featuredprojects`
--
ALTER TABLE `featuredprojects`
MODIFY `feat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
