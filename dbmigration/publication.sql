-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2015 at 11:53 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `price` text NOT NULL,
  `reference` text NOT NULL,
  `category` text NOT NULL,
  `banner` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`id`, `title`, `content`, `price`, `reference`, `category`, `banner`, `active`) VALUES
(7, 'Bird of the Soul', '<p>Ilchi Lee wrote this short, illustrated story to let people know that they can be that free, if only when they reconnect to their souls. It&rsquo;s the story of a man named Jay and his relationship with his soul, symbolized by a sweetly, singing bird. This story is Lee&rsquo;s own personal story, and he believes it is many people&rsquo;s story. Traveling life&rsquo;s journey with Jay, you will rediscover what is most precious and important in life.<br />\n&nbsp;</p>\n\n<p>Accompanying the story are a guided meditation CD with two audio tracks and a 21-Day Meditation Journal to help you reestablish your relationship with your soul. These training sessions guide you in to release and purify your thoughts and emotions and take you on a flight as you fly with your own bird of the soul, enabling you to experience your soul&rsquo;s freedom. Consciously connect and communicate with your soul for 21 days, and your soul will gradually expand and finally take its proper place at the center of your life.</p>\n', '21.95', 'http://www.bestlifemedia.com/body-mind-spirit-books/bird-of-the-soul', '1', '4e262cd9ca3957bb771c27fdaf56e492.jpg', 1),
(8, 'Change', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">A simple shift in perspective can change the course of your entire life. This slice of truth is the impetus behind&nbsp;</span><em>Change</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">. Ilchi Lee writes that we are an intrinsic driver behind the force of change and our creative potential is limitless. This book goes into depth about how unconscious thoughts, behaviors, and emotions have created your life and the world that we live in. It also offers practical methods for delving into your current thought patterns and inserting the ones that will steer your life in the direction you want to go.</span></p>\n', '13.95', 'http://www.bestlifemedia.com/body-mind-spirit-books/font-color-red-new-font-change', '1', '5579e2562da84474bfb9c5f0d5f6b234.jpg', 1),
(9, 'Magnetic Meditation', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Magnetic Meditation is a groundbreaking method of meditation in which you use the tangible sensation of the magnetic fields of magnets to feel, amplify, and circulate energy. Meditating for just 5 minutes with magnets will totally change your meditation experiences. Release stress, regain focus, and recharge your life.</span></p>\n', '13.95', 'http://www.bestlifemedia.com/body-mind-spirit-books/magnetic-meditation-five-minutes-to-health-energy-and-clarity', '1', '7d06070366c48e0c8d97c87ea8a19dc9.jpg', 1),
(10, 'LifeParticle Meditation', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">A meditation book like no other,&nbsp;</span><em>LifeParticle Meditation</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">provides targeted visualization techniques for waking up your mind&rsquo;s abilities and making the changes you want in your life. Rather than being dragged by life&rsquo;s inevitable flow of change, the meditations and ideas in this book allow you to understand, manage, and direct that flow. Join thousands of other LifeParticle Meditation practitioners in using LifeParticles to experience profound healing and self-transformation.</span></p>\n', '16.16', 'http://www.bestlifemedia.com/body-mind-spirit-books/lifeparticle-meditation', '1', 'fe7349896fd3b8644bcfd10d3a8c0233.jpg', 1),
(11, 'The Call of Sedona', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">In Ilchi Lee&rsquo;s newest book, start a journey of spiritual growth and awakening as you follow him in his discovery of the wonders of Sedona, Arizona, a place steeped in earth wisdom. Release the past on the banks of Oak Creek and find a vision of the future among the red rock mountains dotted with cacti and juniper. You will find profound insights about making a true connection with your heart and nature, as well as much practical guidance for experiencing the spirit of Sedona. For more information about this book and ways you can experience Sedona as Ilchi Lee did, please visit</span><a href="http://www.callofsedona.com/" style="margin: 0px; padding: 0px; color: rgb(54, 54, 54); font-family: Arial; font-size: 14.5px; line-height: 18px; background-color: rgb(254, 249, 249);" target="blank">CallofSedona.com</a><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">.</span></p>\n', '16.95', 'http://www.amazon.com/gp/product/1451695802/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=1451695802&linkCode=as2&tag=ilcleepoithew-20', '1', 'cd905701efedd8ee7ac82a91359b4198.jpg', 1),
(12, 'LifeParticle Energy Meditation', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">You can reach a heightened consciousness&mdash;a pure, highly activated and energetic state of being&mdash;through LifeParticle Energy Meditation. Designed by Ilchi Lee and spoken by Jawn McKinley, the guided meditations on this LifeParticle Energy Meditation CD will help you experience unity with the source of life for the transformation of your body, mind, and spirit. Use them to explore a deeper level of self-healing, peace of mind, and unconditional love.</span></p>\n', '16.16', 'http://www.bestlifemedia.com/body-mind-spirit-cds/lifeparticle-energy-meditation-cd', '2', '895cbb51307957dbc5fb21e6b482963b.png', 1),
(13, 'LifeParticle Sound Healing', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">The LifeParticle Sound Healing CD uses sound to amplify the power of LifeParticles and awaken your natural healing abilities. The sounds of the crystal bowls, gongs, flutes, and other instruments, played by Ilchi Lee while in meditation, will generate subtle, yet powerful vibrations through your whole body. These vibrations will calm and purify your mind and emotions. The sounds have the power to activate the body&rsquo;s energy centers and allow LifeParticles to flow powerfully through your body. With consistent practice, your health will improve and negative energies will be released, leaving your body and mind refreshed.</span></p>\n', '17.95', 'http://www.bestlifemedia.com/body-mind-spirit-cds/lifeparticle-sound-healing', '2', 'e50495256b754bafe4692b0dfc44c024.jpg', 1),
(14, 'The Call of Sedona Audio Book', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Millions of people visit Sedona, Arizona each year, drawn by its amazing red rocks, striking sunsets, and sense of peace. But Sedona has much more to offer than its incredible beauty. It has a unique power to open people&rsquo;s eyes to the magnificence of the Earth and their own souls, awakening their consciousness. Interweaving the profound tale of his own experiences in Sedona with practical advice on meditation and spirituality, Ilchi Lee contends that each of us is a great life and a great soul; fulfilling our destiny is a matter of listening to one&rsquo;s inner voice and accepting its message.</span></p>\n', '29.99', 'http://www.amazon.com/gp/product/1442355956/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=1442355956&linkCode=as2&tag=ilcleepoithew-20', '2', 'fd6ee805ee9a70691bdc2789cf4782aa.jpg', 1),
(15, 'Inspiration for Your Day', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Let your spirit soar as you listen to inspirational messages from Ilchi Lee to brighten your day. Derived from his personal meditation sessions, these inspired words of wisdom will help you connect to your own divinity within, the natural environment around you, and the infinite power of the cosmos.</span></p>\n', '17.95', 'http://www.bestlifemedia.com/body-mind-spirit-cds/inspiration-for-your-day-poems', '2', '3dae8205977ee6dd82a336c38a73b59c.jpg', 1),
(16, 'Nature Heals Meditation CD', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Through these meditations drawn from Ilchi Lee&rsquo;s nature meditation tours, immerse yourself in the elemental properties of the planet (rain, water, sun, trees, wind, and earth) and experience the healing energies of Mother Earth. Reawaken your capacity to perceive and communicate with the tremendous world that exists all around and within you. By tapping into natural elements, you can step into the flow of nature and heal your body, mind, and spirit.</span></p>\n', '17.95', 'http://www.bestlifemedia.com/body-mind-spirit-cds/font-color-red-new-font-nature-heals', '2', '3e790464ccdb2b77db8b67f0f6f19a41.jpg', 1),
(17, 'Brain Wave Vibration Guided Training CD', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Manage your stress, relax your body, and stimulate your mind with Brain Wave Vibration, the newest brain fitness method designed by Brain Education founder Ilchi Lee. Let Melissa Koci, a certified Brain Education Instructor, guide you through a complete one-hour Brain Wave Vibration session.</span></p>\n', '17.95', 'http://www.bestlifemedia.com/body-mind-spirit-cds/brain-wave-vibration-guided-training-cd', '2', 'f7cc11ad41f4229c06ce9a163367f549.jpg', 1),
(18, 'Bird of the Soul Essential Oil Roll On', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Enhance every aspect of your life with this illuminating aroma made from the essences of sixteen different plants. Inspired by Ilchi Lee&rsquo;s storybook,&nbsp;</span><em>Bird of the Soul</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">, and mindfully formulated under his guidance, this essential oil can help you soothe and reawaken your inner spirit. As a fragrance for the soul, it&rsquo;s a great addition to your daily yoga or meditation session, heightening the rejuvenating effects of your practice. Inhale the aroma whenever you need to awaken your mind and change your mood and energy.</span></p>\n', '27.50', 'http://www.bestlifemedia.com/body-mind-spirit-gift-store/bird-of-the-soul-essential-oil', '3', '74de6b452b58e3362fea80ccc8aa350e.jpg', 1),
(19, 'Magnetic Meditation Kit', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">Using the push and pull sensation of magnets, Magnetic Meditation makes focusing your mind simpler and easier. Beginners can get into a clear meditative state quickly, and experienced meditators can go deeper than ever before. Kit includes book, 3 hematite magnets, and a velvet carrying pouch.</span></p>\n', '17.96', 'http://www.bestlifemedia.com/body-mind-spirit-gift-store/magnetic-meditation-kit', '3', '6523f5ea852f58101f14dc061ab8be6e.png', 1),
(20, 'CHANGE', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">This dynamic educational film created by&nbsp;</span><em>New York Times</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">bestselling author Ilchi Lee is an offering to anyone who wants to make a positive change in their life. It breaks down the relationship between consciousness and matter, proposing that the two exist as one at the fundamental levels of reality. Thus, reality is changeable. Everything exists as potential, just waiting for you to choose how you want it to present itself in your life. Leaders in the fields of education, medicine, neuroscience, and spirituality such as Neale Donald Walsch, Dr. Stuart Hameroff, and Mariale Hardiman, Ed.D. share their insight and techniques to effect real change in your life and in the world.&nbsp;</span><em>Change</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">&nbsp;empowers you with an invincible hope for yourself and the world. This film is more than just informational&mdash;it&rsquo;s transformational.</span></p>\n', '14.98', 'http://www.bestlifemedia.com/body-mind-spirit-dvds/change-the-lifeparticle-effect', '3', '73cbbec9ad66c968bc06701c8c70afa1.jpg', 1),
(21, 'The Call of Sedona App', '<div>\n<p>A captivating meditation guide full of stunning images and multimedia experiences, Infinity Brain brings the wisdom of Ilchi Lee&rsquo;s book to life. In this app you&rsquo;ll find:</p>\n\n<p>&bull; 8 audio meditations set to music with 24 breathtaking full-screen photographs of Sedona.<br />\n&bull; inspirational messages from the book.<br />\n&bull; a map and directions to Sedona&rsquo;s famous vortex sites.<br />\n&bull; videos introducing you to Sedona and the book.</p>\n</div>\n', '2.99', 'http://www.theinfinitybrain.com/', '3', '0c1121499245b3f817843ce26a1353ae.jpg', 1),
(22, 'Ilchi Calligraphy Note Cards', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial; font-size:14.5px">This set of five natural white note cards and envelopes are a perfect way to share love, peace, oneness, and happiness with inspired calligraphy and wisdom from Ilchi Lee. Ilchi Lee wrote these five sets of Asian characters while in meditation using the flow of universal energy. They are perfect for any occasion.</span></p>\n', '12.95', 'http://www.bestlifemedia.com/body-mind-spirit-gift-store/ilchi-calligraphy-note-cards-set', '3', '97027deb137fb622dba5552a5fde8404.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
