-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2015 at 11:52 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbilchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `album` text NOT NULL,
  `upload_categ` int(11) NOT NULL,
  `path` text NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `duration` text NOT NULL,
  `date` date NOT NULL,
  `language` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `title`, `album`, `upload_categ`, `path`, `description`, `location`, `duration`, `date`, `language`) VALUES
(12, 'How to Change Immediately for a Better Life', 'Public Talks', 0, '<iframe   src="https://www.youtube.com/embed/om9LmASsak4" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">In this clip from his talk at the Korean premiere of his latest film,&nbsp;</span><em>CHANGE: The Brain and Divinity</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, Ilchi Lee shares two simple exercises you can do to immediately change your life. By practicing them regularly you can tap into your natural healing abilities: Plate Balancing Exercise and Toe Tapping. See how it helped one enthusiastic practitioner.</span></p>\n', 'Seoul, South Korea', '03:05', '2015-02-11', 'Korean with English subtitles'),
(14, 'Korean Premiere of the Film CHANGE: The Brain and Divinity', 'Public Talks', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/fkRnCdQuQBU" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Ilchi Lee addressed an audience of VIPs at the Korean premiere of his second film,&nbsp;</span><em>CHANGE: The Brain and Divinity</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">. In this talk, Ilchi Lee shares the secret to accomplishing many things by finding divinity in oneself. He discusses how our environment can affect our focus, and how by learning how to use our Brain Operating System, through which we run our brain, we can develop parts of the brain not used in normal society. By tapping into one&rsquo;s divinity, a person finds their potential.</span></p>\n', 'Seoul, South Korea', '28:49', '2015-01-13', 'Korean with English subtitles'),
(15, 'How to Create Health, Happiness and Peace', 'Public Talks', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/LCbInOzwsDo" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Did you know that health, happiness and peace are all energy? In this talk in Los Angeles, CA, Ilchi Lee shares how recognizing different kinds of energy, and changing your perception can create health, happiness and peace.</span></p>\n', 'Los Angeles, California', '07:03', '2015-01-06', 'English, Korean'),
(16, 'Alzheimerâ€™s, Cell Phones, ADD and the Brain', 'Public Talks', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/D2CWzR7nsnk" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Following his talk at the Union Institute &amp; University, Ilchi Lee answers questions about Alzheimer&rsquo;s and prevention. He also addresses the use of cell phones and technology and releasing addictions through brain refreshing. ADD (Attention Deficit Disorder) may also be addressed when one understands the value of the brain.</span></p>\n', 'Miami, Florida', '12:14', '2015-01-01', 'Following his talk at the Union Institute & University, Ilchi Lee answers questions about Alzheimerâ€™s and prevention. He also addresses the use of cell phones and technology and releasing addictions through brain refreshing. ADD (Attention Deficit Disorder) may also be addressed when one understands the value of the brain.'),
(17, 'Learn How to Observe Your Brain', 'Public Talks', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/Nzv3rHR8itQ" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">At the Union Institute and University in Miami, Ilchi Lee talks about observing your brain through experience. Is yours fluid and flexible? Learn how to focus inside and observe your own brain in order to heal the emotional scars you&rsquo;ve picked up in your life and take control of your destiny.</span></p>\n', 'Miami, Florida', '08:22', '2015-01-24', 'English, Korean'),
(19, 'Use the Divinity of Sage to Awaken Your Consciousness', 'Public Talks', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/ClY-fdyZ5mc" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">In this short clip from his talk at the Korean premiere of his second film,&nbsp;</span><em>CHANGE: The Brain and Divinity</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, Ilchi Lee speaks about the herb Sage and how divinity exists not only in humans, but plants and all things. Sage can help us awaken our consciousness, healing abilities, and character by waking up our senses with its strong scent and flavor. In addition, by becoming aware of the divinity of sage, we can become aware of our own divinity.</span></p>\n', 'Seoul, South Korea', '03:13', '2015-01-23', 'Korean with English subtitle'),
(20, 'Heat for Natural Health', 'Short Clips', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/OO8sm0Al-ig" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Meditation and yoga expert Ilchi Lee talks about how important temperature is for the health of our body and society. The key is the sun, which provides heat without discrimination or cost. If you raise the temperature of your body with the help of the heat of the sun, your body&rsquo;s natural healing ability will be enhanced, and you&rsquo;ll have a passion in your heart for caring for others.</span></p>\n', 'Sedona, Arizona', '03:48', '2014-11-11', 'Korean, English subtitles'),
(21, 'Sedona Nature Meditation: Recognize Your Inner Stillness', 'Short Clips', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/H_cQyXdeuEU" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Peace is already inside you. Recognize it and accept it by letting go of your fears and worries with this simple nature meditation featuring the red rocks and blue skies of Sedona, Arizona.</span></p>\n', 'Sedona, Arizona', '01:47', '2014-04-23', 'English'),
(22, 'Nature Meditation: Serenity at the Summit', 'Short Clips', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/GyBxGzBRFlQ" frameborder="0" allowfullscreen></iframe>', '<p>Become one with nature in this beautiful meditative journey around Sedona. Imagine being there, feel the energy, and you will feel the energy and beauty within yourself.</p>\n\n<p>Reaching the summit is like reaching that calm, quite place inside yourself. Breath deeply and relax. From here you can see things normally too far away. You can sense things you normally may have ignored. When you focus deeply on qi energy inside you. Your mind naturally becomes calm and serene.</p>\n', 'Sedona, Arizona', '01:47', '2014-04-25', 'English'),
(23, 'Bird of the Soul Commercial', 'Short Clips', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/cFoHuNKcLzY" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Take a peak at Ilchi Lee&rsquo;s new book&nbsp;</span><em>Bird of the Soul</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">, a delightful fable for all ages.</span></p>\n', 'Sedona, Arizona', '01:12', '2014-04-25', 'English'),
(24, 'Nature Meditation: Appreciate Your Beauty through Natureâ€™s Energy', 'Short Clips', 0, '<iframe width="700" height="394" src="https://www.youtube.com/embed/gkf2cFu7WNY" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">Ilchi Lee has always been inspired by nature&rsquo;s energy and has shown people how to use it for their personal development and healing. We at Ilchi.com have developed a series of nature meditation videos based on Ilchi Lee&rsquo;s teachings featuring the breathtaking red rock scenery of Sedona, Arizona so you can have the benefits of nature meditation from the comfort of home. This first video in the series reminds us that the land is a living being. If we respect it and recognize its beauty, we can appreciate our own inner beauty.</span></p>\n', 'Sedona, Arizona', '02:13', '2014-03-24', 'English'),
(25, 'Ilchi Lee at the Jakarta 2013 International Film Festival', 'Short Clips', 0, '<iframe width="500" height="281" src="https://www.youtube.com/embed/b93cpDK2rnM?list=UUeCD-KVUIBZcZGsb_Ad5KKg" frameborder="0" allowfullscreen></iframe>', '<p><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">On November 28, 2013, Ilchi Lee attended the International Film Festival for Spirituality, Religion, and Visionary in Jakarta, Indonesia where his film&nbsp;</span><em>CHANGE: The LifeParticle Effect</em><span style="background-color:rgb(254, 249, 249); color:rgb(54, 54, 54); font-family:arial">&nbsp;won 14 awards including an award for Best Short Documentary and a Gold Award for Filmmaker of Spirituality 2013 and Best Story &amp; Concept. See the highlights of the event and hear what Ilchi Lee had to say about it.</span></p>\n', 'Jakarta, Indonesia', '03:41', '2013-11-28', 'English, Korean');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `video`
--
ALTER TABLE `video`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
